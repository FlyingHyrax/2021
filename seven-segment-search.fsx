(*
    Advent of Code 2021
    Day 8 - "Seven Segment Search"
    2022 Matthew Seiler / flyinghyrax.net
*)

open System


(* Domain model types and their helper methods *)


/// Represent individual segments in a 7-segment display by location
type Segment = TT | TL | TR | MM | BL | BR | BB

module Segment =
    let all = [ TT ; TL ; TR ; MM ; BL ; BR ; BB ]


/// Digits that can be displayed on a single 7-segment display
type Digit = D0 | D1 | D2 | D3 | D4 | D5 | D6 | D7 | D8 | D9

module Digit =
    /// All digits in a list in order from smallest to largest
    let all = [D0 ; D1 ; D2 ; D3 ; D4 ; D5 ; D6 ; D7 ; D8 ; D9]

    /// Numeric value of a digit        
    let intValue digit =
        match digit with
        | D0 -> 0
        | D1 -> 1
        | D2 -> 2
        | D3 -> 3
        | D4 -> 4
        | D5 -> 5
        | D6 -> 6
        | D7 -> 7
        | D8 -> 8
        | D9 -> 9

    /// The segments used to display a digit
    let segments digit =
        match digit with
        | D0 -> [| TT; TL; TR;     BL; BR; BB |]
        | D1 -> [|         TR;         BR     |] 
        | D2 -> [| TT;     TR; MM; BL;     BB |]
        | D3 -> [| TT;     TR; MM;     BR; BB |]
        | D4 -> [|     TL; TR; MM;     BR     |]
        | D5 -> [| TT; TL;     MM;     BR; BB |]
        | D6 -> [| TT; TL;     MM; BL; BR; BB |]
        | D7 -> [| TT;     TR;         BR     |]
        | D8 -> [| TT; TL; TR; MM; BL; BR; BB |]
        | D9 -> [| TT; TL; TR; MM;     BR; BB |]
        |> Set.ofArray

    (* Pre-compute the table for 'withSegmentCount' *)
    let _digitsByNumberOfSegments = 
        all |> List.groupBy (segments >> Set.count) |> Map.ofList

    /// Returns the digits that use the specified number of segments
    let withSegmentCount n =
        _digitsByNumberOfSegments
        |> Map.tryFind n
        |> Option.defaultValue List.empty


(* 
    Using our "unique assignment" constraint 
*)

module EnforceUniqueAssignment =
    type AssignmentState =
        { assigned : Map<Segment, char>
        ; unknown : Map<Segment, Set<char>>
        }

    let ignoreKey fn _k v = fn v    

    let isSolved state =
        Map.isEmpty state.unknown && 
        Map.count state.assigned = 7

    /// asssign a value to a segment
    /// (1) adds the segment and its value to the 'assigned' table
    /// (2) removes the value as a possibility from remaining unknown segments
    let assign state segment value =
        
        let removeIt = ignoreKey (Set.remove value)
        
        { assigned = Map.add segment value state.assigned
        ; unknown = Map.map removeIt state.unknown
        }

    /// search table of unknown segments for those with one remaining possibility
    let rec assignSingletons (state: AssignmentState) : AssignmentState =
        // helper functions for working with sets
        let hasOneValue (s : Set<'a>) =
            (Set.count s) = 1

        let getOnlyValue (s : Set<'a>) =
            Set.toList s |> List.head 

        // check the 'unknown' table for segments that have only one possible value.
        let haveOneValue, others =
            Map.partition (ignoreKey hasOneValue) state.unknown

        if Map.isEmpty haveOneValue then
            // BASE case - all segments we know the exact value for are already in
            // the 'assigned' table, there are no segments with one possible value
            // in the 'unknown' table and we don't need to change anything.
            state
        else
            // RECURSIVE case - at least one segment we can assign the value for            
            haveOneValue
            // 1) convert sets of one value to just that value
            |> Map.map (ignoreKey getOnlyValue)
            // 2) move segments with one value from the 'unknown' table to the 'assigned' table
            |> Map.fold assign { state with unknown = others }
            // 3) re-check the updated table for new segments that may now have only one value
            |> assignSingletons

    let constrainAndAssign state constraintFn input =
        let reduced = { state with unknown = constraintFn state.unknown input }
        let assigned = assignSingletons reduced
        assigned

    let constrainWithInputs initial constraintFn inputs =
        let rec loop state remainingInputs =
            if isSolved state then
                state
            else
                match remainingInputs with
                | head::tail ->
                    let state' = constrainAndAssign state constraintFn head
                    loop state' tail
                | _ -> failwith "out of inputs to constrain with!"
        
        loop initial inputs 


(* Reduce possible values for a display segment *)


let reducePossibleValues (segmentsToPossibilies : Map<Segment, Set<char>>) (pattern : Set<char>) =
    let numberOfSegments = Set.count pattern
    let possibleDigits = Digit.withSegmentCount numberOfSegments
    let segmentsInCommon = 
        possibleDigits
        |> List.map Digit.segments
        |> Set.intersectMany

    segmentsToPossibilies
    |> Map.map (fun segment possibleValues ->
        if Set.contains segment segmentsInCommon then
            Set.intersect possibleValues pattern
        else
            possibleValues
    )


(* Problem input types and helper functions *)


// storing the input strings as 'Patterns' of characters
type Pattern = Set<char>

module Pattern =
    let toString p =
        p |> Set.toArray |> Array.sort |> String
    
    let ofString (s : string) =
        s.ToCharArray() |> Set.ofArray

// storing a line of input
type Entry = 
    { examples : Pattern list
    ; current : Pattern list
    }

module Entry =
    
    let parse (line : string) : Entry =
        
        let toPatternList (text : string) : Pattern list =
            text.Split(' ', StringSplitOptions.None)
            |> Array.map Pattern.ofString
            |> Array.toList

        let halves = line.Split('|', StringSplitOptions.TrimEntries)
        
        { examples = toPatternList halves[0]
        ; current = toPatternList halves[1]
        }

    let toString (e : Entry) : string =
        let formatPatterns (ps : Pattern list) : string =
            ps
            |> List.map Pattern.toString
            |> String.concat ", "

        let fmtdExamples = formatPatterns e.examples
        let fmtdCurrent = formatPatterns e.current
        sprintf "Entry{ex= %s ; now= %s}" fmtdExamples fmtdCurrent

    let decode (entry : Entry) : Digit list =
        let segmentsToPossibilities =
            Segment.all
            |> List.map (fun s -> (s, Set.ofList ['a' .. 'g']))
            |> Map.ofList

        let initialState : EnforceUniqueAssignment.AssignmentState = 
            { assigned = Map.empty
            ; unknown = segmentsToPossibilities
            }

        let characterForSegment = 
            EnforceUniqueAssignment.constrainWithInputs initialState reducePossibleValues entry.examples
            |> fun r -> r.assigned
        
        let stringForDigit (d : Digit) =
            Digit.segments d
            |> Set.map (fun s -> Map.find s characterForSegment)
            |> Pattern.toString

        let digitForString =
            Digit.all
            |> List.map (fun d -> (stringForDigit d, d))
            |> Map.ofList

        let displayValues =
            entry.current
            |> List.map Pattern.toString
            |> List.map (fun str -> digitForString[str])

        displayValues


// reading lines of input as strings
let standardInput =
    fun _ -> Console.In.ReadLine()
    |> Seq.initInfinite
    |> Seq.takeWhile ((<>) null)


// read and decode problem inputs
let decodedEntries = 
    standardInput 
    |> Seq.map (Entry.parse >> Entry.decode)
    |> Seq.toList


// how many times to the digits 1, 4, 7, or 8 appear?
let part1 (digits) =
    let count1478 digit =
        match digit with
        | D1 | D4 | D7 | D8 -> 1
        | _ -> 0
    List.sumBy count1478 digits

// what do you get if you add up all the output values?
let part2 (digits) =
    digits
    |> List.map Digit.intValue
    |> List.map (sprintf "%d")
    |> String.concat ""
    |> int

let part1solution = decodedEntries |> List.sumBy part1
let part2solution = decodedEntries |> List.sumBy part2
printfn "Part 1: %d | Part 2: %d" part1solution part2solution
