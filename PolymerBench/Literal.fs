module PolymerBench.Solutions.Literal

/// Performs a single round of polymer string expansion
let expand (rules : Map<char * char, char>) (template : char list) : char list =
    
    let rec loop (expanded : char list) (original : char list) =
        match original with
        | [] -> List.rev expanded
        | [ch] -> loop (ch :: expanded) []
        | chL :: (chR :: rest) ->
            match Map.tryFind (chL, chR) rules with
            | Some chC ->
                loop (chC :: chL :: expanded) (chR :: rest)
            | None ->
                loop (chL :: expanded) (chR :: rest)
    
    loop [] template

/// Perform `count` repeated rounds of polymer expansion using `expand`
let expandN (rules : Map<char * char, char>) (count : int) (template : char list) =
    
    let rec loop (remaining : int) (accumulator : char list) =
        if remaining = 0 then
            accumulator
        else
            loop (remaining - 1) (expand rules accumulator)
    
    loop count template

let countChars (cs : char list) =
    let counts = List.countBy id cs
    let most = List.maxBy snd counts
    let least = List.minBy snd counts
    most, least

open PolymerBench

let solve (rules : Input.RuleSet) (iterations : int) (template : char list) =
    let expandedString = expandN rules iterations template
    let (_, most), (_, least) = countChars expandedString
    most - least
