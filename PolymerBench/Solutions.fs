namespace PolymerBench.Solutions

open PolymerBench

module Recursive =
    let countExpandedChars (rules : Input.RuleSet) (iterations : int) =
        let rec expandAndCount (remainingIterations : int) (pair : char * char)  =
            let left, right = pair
            if remainingIterations = 0 then
                BigCounter.incr2 left right BigCounter.empty
            else
                let center = Map.find pair rules
                let leftSubResult = expandAndCount (remainingIterations - 1) (left, center)
                let rightSubResult = expandAndCount (remainingIterations - 1) (center, right)

                BigCounter.add leftSubResult rightSubResult
                |> BigCounter.decr center

        // return partially applied function, so our caller can just pass a pair of characters and get a result
        expandAndCount iterations


module Memoized =
    
    open System.Collections.Generic
    
    let countExpandedChars (rules : Input.RuleSet) (iterations : int) =
        let cachedResults = Dictionary<int, BigCounter<char>>()
        
        // create a unique key for the cache dictionary that depends on all three arguments
        // to our recursive function, so 2 function calls with different arguments do not
        // resolve to the same value in the cache table.
        let getKey (a : char, b : char) (n : int) : int =
            // in .NET we can cheat and ask the runtime to do a nice tuple hash for us
            (a, b, n).GetHashCode()

        let rec expandAndCount (remainingIterations : int) (pair : char * char) =
            let cacheKey = getKey pair remainingIterations

            if cachedResults.ContainsKey(cacheKey) then
                // if we already have a result for these arguments, return it from the cache
                // instead of re-calculating; this prevents evaluating the same arguments repeatedly.
                cachedResults[cacheKey]
            else
                // if we need to calculate a result, the algorithm is the same as without memoization, but
                // (1) we call ourselves and not the un-memoized version and
                // (2) we cache the calculated result for re-use before returning it
                let left, right = pair
                if remainingIterations = 0 then
                    BigCounter.incr2 left right BigCounter.empty
                else
                    let center = Map.find pair rules
                    let leftSubtree = expandAndCount (remainingIterations - 1) (left, center)
                    let rightSubtree = expandAndCount (remainingIterations - 1) (center, right)
                    let result = 
                        BigCounter.add leftSubtree rightSubtree
                        |> BigCounter.decr center
                    
                    // cache the result before returning it, so calls with the same arguments
                    // do not have to re-calculate it.
                    cachedResults[cacheKey] <- result
                    result

        // yay partial function application
        expandAndCount iterations

module Incremental =

    // store the accumulated counts of each character after expanding a pair some number of times.
    // pair is the key, value is the accumulated result for that pair.
    type CountAccumulator = Map<char * char, BigCounter<char>>

    let countExpandedChars (rules : Input.RuleSet) (iterations : int) =

        // for our known problem sets, the rules perfectly cover all 2-character combinations,
        // so we don't have to calculate the alphabet somehow
        let allCharacterPairs = Map.keys rules

        // start with a mapping where the pairs haven't been expanded at all; after 0 iterations
        // the number of characters for that pair only includes the 2 characters in the pair
        let initialAccumulator : CountAccumulator =
            allCharacterPairs
            |> Seq.map (fun pair ->
                let baseCount =
                    BigCounter.empty
                    |> BigCounter.incr (fst pair)
                    |> BigCounter.incr (snd pair)
                pair, baseCount
            )
            |> Map.ofSeq

        // here's our core iterative algorithm; on every iteration we build the next accumulator
        // by adding results from the previous one. The execution time is bounded by C * N * M 
        // where C is how long it takes to execute the function once, N is  totalIterations, 
        // and M is the number of possible character pairs; and the memory usage is bounded to 
        // 2 * sizeof(CountAccumulator). There is also no stack expansion because the only
        // recursive functions are tail-call optimized, which is something we could not do
        // with our previous solutions where the recurrence was not in tail call position.
        let rec generateNext (iteration : int) (current : CountAccumulator) =
            
            // a helper function to play nice with Seq.map + Map.ofSeq
            let nextForPair (pair : char * char) =
                let l, r = pair
                let c = Map.find (l, r) rules
                let leftPrevResult = Map.find (l, c) current
                let rightPrevResult = Map.find (c, r) current
                let nextResult =
                    BigCounter.add leftPrevResult rightPrevResult
                    |> BigCounter.decr c
                pair, nextResult
                
            if iterations - iteration = 0 then
                current
            else
                let next =
                    allCharacterPairs
                    |> Seq.map nextForPair
                    |> Map.ofSeq

                // perform the next iteration; this will be converted to a loop by TCO
                generateNext (iteration + 1) next

        // now THIS mapping contains the result of expanding every possible pair of characters up to `totalIterations`!
        let expandedCounts = 
            generateNext 0 initialAccumulator

        // At this point, totalling things up is the same as SolveRecursive.solveWith, so we just need to have a signature
        // that matches what that function expects and we can re-use it! We've already calculated a result for each possible
        // pair of characters, so our ExpansionFunction, instead of actually evaluating anything, can just look it up.

        fun (pair : char * char) ->
            expandedCounts[pair]