module PolymerBench.Input

open FParsec
open System

// We don't need FParsec's user data capability, so add a type alias to
// remove it from our function signatures etc.
type Parser<'T> = Parser<'T, unit>

type Rule = (char * char) * char
type RuleSet = Map<char * char, char>

let parseRule : Parser<Rule> =
    let pair = upper .>>. upper
    let arrow = skipString " -> "
    let insert = upper
    pair .>> arrow .>>. insert

let parseRules : Parser<RuleSet> =
    sepEndBy1 parseRule newline
    |>> Map.ofList

let parseInput : Parser<char list * RuleSet> =
    let header =
        many1 upper .>> newline .>> newline

    header .>>. parseRules

let runParser (input : IO.Stream) =
    let result =
        runParserOnStream parseInput () "" input Text.Encoding.UTF8
    match result with
    | Success (data, _, _) -> data
    | Failure (msg, err, _) ->
        failwithf "failed to parse: %s : %A" msg err
