namespace PolymerBench

type BigCounter<'T when 'T: comparison> = Map<'T, bigint>

module BigCounter =

    /// Returns an empty count map
    let empty : BigCounter<'T> =
        Map.empty

    /// Returns a count map containing the specified keys initialized to 0
    let zeroWithKeys (keySet : Set<'T>) : BigCounter<'T> =
        keySet
        |> Set.toSeq
        |> Seq.map (fun k -> (k, 0I))
        |> Map.ofSeq

    /// Increment the count for a key. If the key was not previously in the count map,
    /// it is added with an initial count of 1.
    let incr (key : 'T) (counter : BigCounter<'T>) =
        Map.change key (fun current ->
            match current with
            | Some count -> Some (count + 1I)
            | None -> Some 1I
        ) counter

    /// Increment the count for 2 keys.
    /// (I'm "cheating" and know that this would be useful from a previous version of my solution.)
    let incr2 (k1 : 'T) (k2 : 'T) (counter : BigCounter<'T>) : BigCounter<'T> =
        counter
        |> incr k1
        |> incr k2

    /// Decrement the count for a key. Throws an exception if the key has a count of 0 or is not in the map,
    /// because in our algorithms this should not happen and would indicate a bug.
    let decr (key : 'T) (counter : BigCounter<'T>) : BigCounter<'T> =
        Map.change key (fun current ->
            match current with
            | Some count ->
                if count <= 0I then
                    failwithf "tried to decrement count for key %A but it was already at zero" key
                else
                    Some (count - 1I)
            | None ->
                failwithf "tried to decrement count for key %A but it's not in the map" key
        ) counter

    /// Combine the counts from 2 count maps. Adds the counts for keys that occur in both maps;
    /// keys that only occur in one map are added to the result with their count from the map
    /// they occur in.
    let add (left : BigCounter<'T>) (right : BigCounter<'T>) : BigCounter<'T> =
        
        // helper function for getting the current count for a key when
        // the target map might not have a value for it
        let safeGet (key : 'T) (cx : BigCounter<'T>) : bigint =
            Map.tryFind key cx
            |> Option.defaultValue 0I

        // helper function for creating a (key, value) tuple that adds the counts from both maps
        // for the given key; the output of this slots nicely into `Map.ofList`.
        let addKey (key : 'T) =
            let lv = safeGet key left
            let rv = safeGet key right
            (key, lv + rv)
        
        // the left and right maps may have different keys, combine their keys
        let keySet : Set<'T> =
            Set.union
                (Map.keys left |> Set.ofSeq)
                (Map.keys right |> Set.ofSeq)

        // add the counts for each key, creating a new count map with the results
        keySet
        |> Set.toList
        |> List.map addKey
        |> Map.ofList