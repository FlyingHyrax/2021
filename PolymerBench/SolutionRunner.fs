module PolymerBench.Runner

open PolymerBench

// List.windowed yields the windows as lists and not tuples and we can't
// convert an arbitrary number of arguments into a tuple; so this helper
// is just to change 'List<List<char>>' into 'List<char * char>'
let toTuple (xs : 'a list) =
    xs[0], xs[1]

/// A function that, given the pair expansion rule set and a number of iterations,
/// counts the number of occurences of each character after expanding a given pair
/// of characters that many iterations.
type ExpansionFunction = 
    Input.RuleSet -> int -> (char * char) -> BigCounter<char>

let solveWith (expandFn : ExpansionFunction) (rules : Input.RuleSet) (iterations : int) (template : char list) =
    let partialExpandFn = 
        expandFn rules iterations
    
    let countWithOverlap (index : int, pair : (char * char)) =
        let result = partialExpandFn pair
        if index = 0 then
            result
        else
            BigCounter.decr (fst pair) result

    template
    |> List.windowed 2
    |> List.map toTuple
    |> List.indexed
    |> List.map countWithOverlap
    |> List.fold BigCounter.add BigCounter.empty
    |> Map.toList
    |> fun counts ->
        let (_, least) = List.minBy snd counts
        let (_, most) = List.maxBy snd counts
        most - least