﻿
open System
open PolymerBench
open PolymerBench.Solutions
open BenchmarkDotNet.Attributes
open BenchmarkDotNet.Running

(*
    I had corresponding modules for each of the following parameter types.
    BenchmarkDotNet's default out-of-process toolchain somehow leaves behind or 
    optimizes away submodules of the 'Program' module, because if we stored the 
    ParamsSource values outside the class we would get a either a null reference
    or target does not exist exception depending on the exact configuration. The
    exception occured even before any GlobalSetup method. Using the in-process
    toolchain or enabling DebugInProcess caused the problem to go away and the
    benchmark cases would be discovered and run correctly.

    I should make a minimal reproducible example for this.
*)

type InputData =
    { Name : string
    ; Template : char list
    ; Rules : Input.RuleSet
    }
    with 
        override self.ToString() = 
            self.Name

type SolutionAlgorithm =
    { Name : string
    ; Func : Runner.ExpansionFunction
    }
    with
        override self.ToString() =
            self.Name



// Without using InProcessEmitToolchain (enabled with the "InProcessAttribute"), 
// all bunchmarks fail at runtime and I'm unsure why
[<MemoryDiagnoser>]
[<MediumRunJob>]
[<KeepBenchmarkFiles>]
[<StopOnFirstError>]
type public Benchmarks() =

    let basePath = @"C:\Users\mrsei\source\advent of code\2021\inputs\day14"

    let inputFiles = 
        [ "tiny.txt"
        ; "sample.txt"
        ; "full.txt"
        ]

    let openFileStream (path : string) : IO.Stream =
        new IO.FileStream(path, IO.FileMode.Open, IO.FileAccess.Read) :> IO.Stream

    let fromFile (name : string) =
        let fullPath = IO.Path.Join(basePath, name)
        use stream = openFileStream fullPath
        let template, rules = Input.runParser stream
        { Name = name ; Template = template ; Rules = rules }

    member public _.InputDataValues : InputData list =
        [ for file in inputFiles -> fromFile file ]

    member public _.AlgorithmValues : SolutionAlgorithm list =
        [ { Name = "recursive"; Func = Recursive.countExpandedChars }
        ; { Name = "memoized"; Func = Memoized.countExpandedChars } 
        ; { Name = "incremental"; Func = Incremental.countExpandedChars } 
        ]

    [<Params(5, 10, 20)>]
    member val public Iterations = 0 with get, set

    [<ParamsSource("InputDataValues")>]
    member val public InputData : InputData = 
        { Name = "" ; Template = [] ; Rules = Map.empty } with get, set

    [<ParamsSource("AlgorithmValues")>]
    member val public Algorithm : SolutionAlgorithm = 
        { Name = "" ; Func = fun _ _ _ -> BigCounter.empty } with get, set

    [<Benchmark>]
    member public self.RunSolver () =
        let result =
            Runner.solveWith 
                self.Algorithm.Func
                self.InputData.Rules
                self.Iterations
                self.InputData.Template
        result


printfn "Starting"

let summary = BenchmarkRunner.Run<Benchmarks>()

printfn "Complete"
