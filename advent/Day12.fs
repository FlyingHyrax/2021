[<adventlib.Register.Module>]
module advent.Day12

open adventlib
open System

/// The kind of cave represented by the graph node
type Kind =
    | Start
    | End
    | Big
    | Small

module Kind =
    /// We can tell what kind of cave a label represents based on the label itself.
    /// The special names "start" and "end" represent the start and end nodes, 
    /// labels formed with uppercase characters represent large caves, and labels
    /// formed with lowercase characters represent small caves
    let ofLabel (id : string) =
        match id with
        | "start" -> Start
        | "end" -> End
        | other ->
            let isUppercase = 
                other.ToCharArray() 
                |> Array.head 
                |> Char.IsUpper
            
            if isUppercase then
                Big
            else
                Small

/// Store the graph using a map from node labels to their reachable neighbors (adjacency lists)
type Graph = Map<string, Set<string>>

module Graph =

    let findOrCreateNode (label : string) (graph : Graph) : Set<string> =
        Map.tryFind label graph
        |> Option.defaultValue Set.empty

    let addDirectional (src : string) (dst : string) (graph : Graph) : Graph =
        let currentNeighbors = findOrCreateNode src graph
        let updatedNeighbors = Set.add dst currentNeighbors
        Map.add src updatedNeighbors graph
    
    let addBidirectional (left : string) (right : string) (graph : Graph) : Graph =
        graph
        |> addDirectional left right
        |> addDirectional right left

    /// Function to convert our input to our structured types. Start with an empty Map
    /// and fold over the input lines, adding an edge to the graph for each line, and 
    /// creating nodes as they are encountered in the input.
    let build (lines : string list) : Graph =
        
        /// Split a line of input into two node labels
        let toEdge (line : string) =
            let parts = line.Split('-')
            parts.[0], parts.[1]

        /// Folder to add an edge to the graph given the labels of the nodes.
        let addEdge (graph : Graph) (left : string, right : string) : Graph =
            // choose the kind of edge to add and its direction based on the node types
            let leftKind = Kind.ofLabel left
            let rightKind = Kind.ofLabel right

            // start node gets only out-edges, b/c there's no point going back to the start.
            // end node gets only in-edges, b/c there's no point leaving the end.
            // all other edges are treated as bidirectional.
            let updateFunc =
                match (leftKind, rightKind) with
                | Start, _ | _, End -> addDirectional left right
                | _, Start | End, _ -> addDirectional right left
                | _ -> addBidirectional left right

            updateFunc graph
            

        lines
        |> List.map toEdge
        |> List.fold addEdge Map.empty 


// F# doesn't have typeclasses, so we'll use interfaces
type IPath =
    abstract member Tip : string
    abstract member Append : string -> IPath option

module Part1 =

    // In part1 we only need to track small caves we visited before
    type Path =
        { Nodes : string list
        ; Visited : Set<string>
        }

        interface IPath with

            member self.Tip =
                List.head self.Nodes

            // When adding a new node to a path, we can always add a big cave or the end,
            // can never add start (because we can't go back after leaving in the first move),
            // and can add a small cave if we haven't visited it before.
            member self.Append (node : string) =
                let kind = Kind.ofLabel node
                match kind with
                | Start -> None
                | Big | End ->
                    Some { self with Nodes = node::self.Nodes }
                | Small ->
                    if Set.contains node self.Visited then
                        None
                    else
                        Some { self with 
                                Nodes = node::self.Nodes ; 
                                Visited = Set.add node self.Visited }

    let start =
        { Nodes = ["start"] 
        ; Visited = Set.empty 
        }

module Part2 =

    // In part 2, we need to keep track of all the small caves that we've visited before,
    // as well as a single small cave we've visited twice. Rules for large caves are identical.
    type Path =
        { Nodes : string list 
        ; VisitedOnce : Set<string>
        ; VisitedTwice : string option
        }

        interface IPath with

            member self.Tip =
                List.head self.Nodes

            // Rules for adding nodes to a path in part 2 are mostly the same, but we can visit
            // a single small cave a second time, so we check if we've visited a small caves
            // and also check if we've visited any cave twice.
            member self.Append (node : string) =
                let nodeKind = Kind.ofLabel node
                match nodeKind with
                | Start -> 
                    // can't append start - once we leave we can't go back.
                    // (this is already covered by our use of directed edges,
                    // but adding it anyway to completely match the union type.)
                    None
                | End | Big -> 
                    // We can always navigate into big caves or the end cave
                    Some { self with Nodes = node::self.Nodes }
                | Small ->
                    // for small caves, it depends!
                    let visitedOnce = Set.contains node self.VisitedOnce
                    match (visitedOnce, self.VisitedTwice) with
                    | false, _ ->
                        // never visited this node, update path and visited set
                        Some { self with 
                                    Nodes = node::self.Nodes ;
                                    VisitedOnce = Set.add node self.VisitedOnce }
                    | true, None ->
                        // we have visited this node once, but no small node has been visited twice,
                        // so we can take that spot:
                        Some { self with
                                Nodes = node::self.Nodes ;
                                VisitedTwice = Some node }
                    | _ ->
                        // only remaining case is that we've visited the node once before but
                        // another node has already been visited twice, so we can't continue
                        None

    let start =
        { Nodes = ["start"] 
        ; VisitedOnce = Set.empty 
        ; VisitedTwice = None 
        }


let countPaths<'a when 'a :> IPath> (graph : Graph) (start : 'a) =
    let isComplete (p : 'a) : bool =
        Kind.ofLabel p.Tip = End

    let extend (path : 'a) : 'a list =
        // get neighbord 
        graph[path.Tip]
        // convert to list b/c we want to use List.choose
        |> Set.toList
        // create new paths, may include None's
        |> List.map path.Append
        // filter out the None's, and extract the values from the Some's
        // FIXME:  don't like the runtime downcast...
        |> List.choose (Option.map (fun p -> p :?> 'a))
    
    let rec loop (growingPaths : 'a list) (totalCompleted : int) =
        match growingPaths with
        | [] -> totalCompleted
        | _ ->
            let extendedPaths =
                growingPaths
                |> List.map extend
                |> List.concat
            
            let incompletePaths =
                extendedPaths
                |> List.filter (isComplete >> not)

            let completedPaths =
                List.length extendedPaths - List.length incompletePaths

            loop incompletePaths (totalCompleted + completedPaths)

    loop [ start ] 0


[<Register.Solution(12, Description="Passage Pathing")>]
let solve (input : IO.Stream) : Map<string, string> =
    // to begin, just convert the input and print it
    let graph =
        input
        |> Input.getLines
        |> Graph.build
    
    // printfn "%A" graph
    
    let p1 = countPaths graph Part1.start
    let p2 = countPaths graph Part2.start

    Map.ofList [ 
        ("Part 1", string p1) ;
        ("PArt 2", string p2)
    ]
