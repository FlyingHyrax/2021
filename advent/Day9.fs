namespace advent

open adventlib



[<Register.Module>]
module Day9 =

    let charForDigit = Map.ofList [
        (0, '0') ;
        (1, '1') ;
        (2, '2') ;
        (3, '3') ;
        (4, '4') ;
        (5, '5') ;
        (6, '6') ;
        (7, '7') ;
        (8, '8') ;
        (9, '9') ;
    ]

    // 1st dimension is row, second dimension is column
    let readHeightMap (lines : string list) =
        lines
        |> List.map Array.ofSeq
        |> List.map (Array.map (string >> int))
        |> array2D

    // return lowest and highest valid indices for a 2d array 
    // as a pair of pairs (D1min, D1max), (D2min, D2max)
    let dimensions (matrix : 'a[,]) =
        let minRow = Array2D.base1 matrix
        let minCol = Array2D.base2 matrix

        let maxRow = (Array2D.length1 matrix) - 1
        let maxCol = (Array2D.length2 matrix) - 1

        (minRow, maxRow), (minCol, maxCol)

    // check if a 2D index in valid in the given 2D array
    let pointInBounds (matrix : 'a[,]) (row, col) =
        let (minRow, maxRow), (minCol, maxCol) = dimensions matrix

        let incl l h v =
            l <= v && v <= h

        let inRow = incl minRow maxRow
        let inCol = incl minCol maxCol

        inRow row && inCol col

    // return list of 2D indices considered adjacent to the given 2D index
    // (does NOT check for boundaries!)
    let adjacentCoords r c =
        [ (r + 1, c) ; (r - 1, c) ; (r, c + 1) ; (r, c - 1) ]

    // find points in the given height map that are lower than all their adjacent points
    // and return as a list of "coordinates" ((row, column) index pairs)
    let findLowPoints (heightMap : int[,]) =
        
        let (minRow, maxRow), (minCol, maxCol) = dimensions heightMap

        let neighbors r c =
            adjacentCoords r c
            |> List.filter (pointInBounds heightMap)
            |> List.map (fun (r, c) -> heightMap[r, c])

        seq {
            for r in minRow..maxRow do
                for c in minCol..maxCol do
                    let height = heightMap[r, c]
                    let isLowest = neighbors r c
                                    |> List.forall (fun nbr -> height < nbr)
                    if isLowest then
                        yield (r, c)
        }
        

    // solve part 1 - find all the low points, and calculate "risk" as the sum of each (height + 1)
    let totalLowPointRisk (heightMap : int[,]) =
        findLowPoints heightMap
        |> Seq.map (fun (r, c) -> heightMap[r, c] + 1)
        |> Seq.sum

    type Basin =
        | Unassigned
        | Ridge
        | Basin of int

    let createBasinMap (heights : int[,]) =
        // label basins starting at 1, so 0 indicates "not a basin"
        let nextId =
            let mutable counter = 0
            fun () ->
                counter <- counter + 1
                counter
        
        // create a second array for tracking that points are in what basins
        let basins = Array2D.create (Array2D.length1 heights) (Array2D.length2 heights) Basin.Unassigned
        // initialize the basins map by finding low points (each the bottom of a disjoint basin)
        // and assigning them consecutive numbers
        findLowPoints heights
        |> Seq.iteri (fun i (r, c) -> 
            basins[r,c] <- Basin (i + 1))

        let lowestNeighborOf r c =
            adjacentCoords r c
            |> List.filter (pointInBounds heights)
            |> List.minBy (fun (r, c) -> heights[r, c])

        // the basin a cell belongs to, is the same basin as its lowest neighbor,
        // unless it is a ridge, then it has no basin
        let rec basinOf row col current =
            match current with
            | Ridge | Basin _ -> 
                // if this cell already has a basin assigned, or is already marked as a ridge,
                // we don't need to do anything and can just return that indicator
                current
            | Unassigned -> 
                // if we don't know the basin for this cell yet, then find it by finding the basin of its lowest neighbor
                // important that we update the map at this point to prevent rework on recursions
                let newValue = if heights[row, col] = 9 then
                                    Ridge
                                else
                                    lowestNeighborOf row col
                                    |> fun (nr, nc) -> basinOf nr nc basins[nr, nc]
                basins[row, col] <- newValue
                newValue
                
        // change the initial value for each cell to the basin it should be part of
        Array2D.iteri (fun r c b -> basinOf r c b |> ignore) basins
        basins
    
    let calculateBasinSizes (basins : Basin[,]) =
        // we no longer care about the structure of the basins, only how many of each we have.
        // so we can convert to a linear sequence to count members
        let justId b =
            match b with
            | Basin n -> n
            | _ -> -1

        basins
        |> Seq.cast<Basin>
        |> Seq.map justId
        |> Seq.filter (fun n -> n > 0)
        |> Seq.countBy id

    // used for visually validating 'createBasinMap'
    let printBasins (basins : Basin[,]) =
        let cs = Array2D.map (fun b -> 
                    match b with
                    | Unassigned -> '?'
                    | Ridge -> '^'
                    | Basin n -> charForDigit[n]
                    ) basins
        seq {
            for r in (Array2D.base1 cs)..(Array2D.length1 cs)-1 do
                let rowChars = cs[r,*]
                yield System.String(rowChars)
        }
        |> Seq.iter (printfn "%s")

        
    [<Register.Solution(9, Description="Smoke Basin")>]
    let solve input : Map<string, string> =
        let heightMap = input |> Input.getLines |> readHeightMap
        let risk = totalLowPointRisk heightMap

        let basinScore = 
            createBasinMap heightMap
            |> calculateBasinSizes
            |> Seq.map snd
            |> Seq.sortDescending
            |> Seq.take 3
            |> Seq.reduce (*)
        
        Map.ofList [ 
            ("Part 1", string risk) ;
            ("Part 2", string basinScore) ;
        ]