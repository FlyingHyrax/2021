[<adventlib.Register.Module>]
module advent.Day14

open adventlib
open System
open FParsec

[<Literal>]
let DUMP_RECURSION = false

type Parser<'t> = Parser<'t, unit>

type Rule = (char * char) * char

let parseRule : Parser<Rule> =
    let pAdj = upper .>>. upper
    let pSep = skipString " -> "
    let pIns = upper
    pAdj .>> pSep .>>. pIns

let parseInput (input : IO.Stream) =
    let pTemplate = 
        many1 upper .>> newline .>> newline

    let pRules =
        sepEndBy1 parseRule newline
        |>> Map.ofList

    let pInput =
        pTemplate .>>. pRules

    runParserOnStream pInput () "input" input Text.Encoding.UTF8

/// "Breadth first" string expansion that literally builds the string for each expansion step.
/// For anything more than about 20 iterations this becomes infeasible.
module SolveLiteral =
    
    /// Performs a single round of polymer string expansion
    let expand (rules : Map<char * char, char>) (template : char list) : char list =
        
        let rec loop (expanded : char list) (original : char list) =
            match original with
            | [] -> List.rev expanded
            | [ch] -> loop (ch :: expanded) []
            | chL :: (chR :: rest) ->
                match Map.tryFind (chL, chR) rules with
                | Some chC ->
                    loop (chC :: chL :: expanded) (chR :: rest)
                | None ->
                    loop (chL :: expanded) (chR :: rest)
        
        loop [] template

    /// Perform `count` repeated rounds of polymer expansion using `expand`
    let expandN (rules : Map<char * char, char>) (count : int) (template : char list) =
        
        let rec loop (remaining : int) (accumulator : char list) =
            if remaining = 0 then
                accumulator
            else
                loop (remaining - 1) (expand rules accumulator)
        
        loop count template

    let countChars (cs : char list) =
        let counts = List.countBy id cs
        let most = List.maxBy snd counts
        let least = List.minBy snd counts
        most, least


/// For part 2, the count of each character may be very large. Likely this will fit in uint64, but why worry?
type BigCounter<'T when 'T: comparison> = Map<'T, bigint>

module BigCounter =

    /// Returns an empty count map
    let empty : BigCounter<'T> =
        Map.empty

    /// Returns a count map containing the specified keys initialized to 0
    let zeroWithKeys (keySet : Set<'T>) : BigCounter<'T> =
        keySet
        |> Set.toSeq
        |> Seq.map (fun k -> (k, 0I))
        |> Map.ofSeq

    /// Increment the count for a key. If the key was not previously in the count map,
    /// it is added with an initial count of 1.
    let incr (key : 'T) (counter : BigCounter<'T>) =
        Map.change key (fun current ->
            match current with
            | Some count -> Some (count + 1I)
            | None -> Some 1I
        ) counter

    /// Increment the count for 2 keys.
    /// (I'm "cheating" and know that this would be useful from a previous version of my solution.)
    let incr2 (k1 : 'T) (k2 : 'T) (counter : BigCounter<'T>) : BigCounter<'T> =
        counter
        |> incr k1
        |> incr k2

    /// Decrement the count for a key. Throws an exception if the key has a count of 0 or is not in the map,
    /// because in our algorithms this should not happen and would indicate a bug.
    let decr (key : 'T) (counter : BigCounter<'T>) : BigCounter<'T> =
        Map.change key (fun current ->
            match current with
            | Some count ->
                if count <= 0I then
                    failwithf "tried to decrement count for key %A but it was already at zero" key
                else
                    Some (count - 1I)
            | None ->
                failwithf "tried to decrement count for key %A but it's not in the map" key
        ) counter

    /// Combine the counts from 2 count maps. Adds the counts for keys that occur in both maps;
    /// keys that only occur in one map are added to the result with their count from the map
    /// they occur in.
    let add (left : BigCounter<'T>) (right : BigCounter<'T>) : BigCounter<'T> =
        
        // helper function for getting the current count for a key when
        // the target map might not have a value for it
        let safeGet (key : 'T) (cx : BigCounter<'T>) : bigint =
            Map.tryFind key cx
            |> Option.defaultValue 0I

        // helper function for creating a (key, value) tuple that adds the counts from both maps
        // for the given key; the output of this slots nicely into `Map.ofList`.
        let addKey (key : 'T) =
            let lv = safeGet key left
            let rv = safeGet key right
            (key, lv + rv)
        
        // the left and right maps may have different keys, combine their keys
        let keySet : Set<'T> =
            Set.union
                (Map.keys left |> Set.ofSeq)
                (Map.keys right |> Set.ofSeq)

        // add the counts for each key, creating a new count map with the results
        keySet
        |> Set.toList
        |> List.map addKey
        |> Map.ofList

(* 
    For the more interesting solutions that can scale to a higher number of iterations, we won't actually generate the
    expanded string. Instead, we will only calculate the number of times each character *would* occur in the expanded
    string. i.e. we don't actually write out the string, but instead use the expansion rules to calculate the number
    of times each character will occur without actually writing them all down.

    The way we do this is with a *recurrence relation*. We can write a recursive formula that describes the answer we 
    want, then implement it with recursive functions. We'll keep track the number of times each character occurs
    using a dictionary, mapping each character to a number, like `{A: 1, B: 2, C: 0}`. We also need to define some
    operations for combining these results with addition or subtraction:

        C1 + C2 =
        { ∀ k ⊂ ks : (k, C1[k] + C2[k]) }
        where
            keys(C_) = the keys in map C_
            ks = keys(C1) ⋃ keys(C2)
            C_[k] = the value stored in map C_ for key k, or 0 if k is not in C_

        (We merge the maps together, adding the values where they have the same keys.)

        C1 - C2 =
        { ∀ k ⊂ keys(c2) : (k, C1[k] - C2[k]) }
        where
            (same definitions as above, note we drop keys in set keys(C2) - keys(C1))
            
    OKay with that stuff defined, here's the recurrence we're working with:

        count(L, R, N) = 
            count(L, C, N - 1) + count(C, R, N - 1) - {C: 1}
                where C = rules[(L, R)]
        
        count(L, R, 0) =
            {L: 1, R: 1}

    A few examples would go a ways here... and this is also turning into some kind of blog post.
*)

/// Solve the problem by instead only storing the number of times each character occurs for a given level of expansion.
/// ...(TODO: describe)
/// Note how this implementation is *not* tail recursive and re-solves for the same arguments repeatedly,
/// i.e. has overlapping subproblems. Although this solution is not practical for large input sizes, we can use it
/// to validate the correctness of the algorithm in theory by comparing its output to the "literal" implementation
/// in part 1 (a "test oracle"). If the recursive algorithm is correct, we can optimize it for practical use via
/// memoization (trade space for time) or dynamic programming (converting to a table-based iterative version).
module SolveRecursive =
    
    // List.windowed yields the windows as lists and not tuples and we can't
    // convert an arbitrary number of arguments into a tuple; so this helper
    // is just to change 'List<List<char>>' into 'List<char * char>'
    let toTuple (xs : 'a list) =
        xs[0], xs[1]

    /// A function that, given the pair expansion rule set and a number of iterations,
    /// counts the number of occurences of each character after expanding a given pair
    /// of characters that many iterations.
    type ExpansionFunction = 
        Map<char * char, char> -> int -> (char * char) -> BigCounter<char>

    let solveWith (expandFn : ExpansionFunction) (rules : Map<char * char, char>) (iterations : int) (template : char list) =
        let partialExpandFn = 
            expandFn rules iterations
        
        let countWithOverlap (index : int, pair : (char * char)) =
            let result = partialExpandFn pair
            if index = 0 then
                result
            else
                BigCounter.decr (fst pair) result

        template
        |> List.windowed 2
        |> List.map toTuple
        |> List.indexed
        |> List.map countWithOverlap
        |> List.fold BigCounter.add BigCounter.empty

    

    /// Non-memoized, recursive character counter.
    /// Given a pair of character (L, R), we can check the rule set to get the character C that will be inserted between them, (L, C, R).
    /// This triple can be split into 2 subpairs : (L, C) and (C, R). If we count the number of occurrences for each subpair, then the
    /// number of occurrences for the original pair is the solution for (L, C) + the solution for (C, R) - one occurrence of the character
    /// C because the left and right subpairs overlap in the final string.
    let recursiveCounter (rules : Map<char * char, char>) (totalIterations : int) =
        
        let rec expandAndCount (remainingIterations : int) (pair : char * char)  =
            let left, right = pair
            if remainingIterations = 0 then
                BigCounter.incr2 left right BigCounter.empty
            else
                let center = Map.find pair rules
                let leftSubResult = expandAndCount (remainingIterations - 1) (left, center)
                let rightSubResult = expandAndCount (remainingIterations - 1) (center, right)

                BigCounter.add leftSubResult rightSubResult
                |> BigCounter.decr center

        // return partially applied function, so our caller can just pass a pair of characters and get a result
        expandAndCount totalIterations

    // Using mutable 'Dictionary' collection for memoization table
    open System.Collections.Generic

    /// The same as recursiveCounter, but memoized. This trades space (the cache in memory) for time 
    /// (spent re-solving overlapping subproblems many times.) If memory is not a constraint and our
    /// subproblems overlap a lot (...which they do) then this can be very efficient.
    let memoizedCounter (rules : Map<char * char, char>) (totalIterations : int) =
        let cachedResults = Dictionary<int, BigCounter<char>>()
        
        // create a unique key for the cache dictionary that depends on all three arguments
        // to our recursive function, so 2 function calls with different arguments do not
        // resolve to the same value in the cache table.
        let getKey (a : char, b : char) (n : int) : int =
            // in .NET we can cheat and ask the runtime to do a nice tuple hash for us
            (a, b, n).GetHashCode()

        let rec expandAndCount (remainingIterations : int) (pair : char * char) =
            let cacheKey = getKey pair remainingIterations

            if cachedResults.ContainsKey(cacheKey) then
                // if we already have a result for these arguments, return it from the cache
                // instead of re-calculating; this prevents evaluating the same arguments repeatedly.
                cachedResults[cacheKey]
            else
                // if we need to calculate a result, the algorithm is the same as without memoization, but
                // (1) we call ourselves and not the un-memoized version and
                // (2) we cache the calculated result for re-use before returning it
                let left, right = pair
                if remainingIterations = 0 then
                    BigCounter.incr2 left right BigCounter.empty
                else
                    let center = Map.find pair rules
                    let leftSubtree = expandAndCount (remainingIterations - 1) (left, center)
                    let rightSubtree = expandAndCount (remainingIterations - 1) (center, right)
                    let result = 
                        BigCounter.add leftSubtree rightSubtree
                        |> BigCounter.decr center
                    
                    // cache the result before returning it, so calls with the same arguments
                    // do not have to re-calculate it.
                    cachedResults[cacheKey] <- result
                    result

        // yay partial function application
        expandAndCount totalIterations


/// This is the "dynamic programming" version of SolveRecursive. It takes the same algorithm and flips it upside down,
/// solving the recursive base cases first and building upward to create the counts for the requested number of expansions.
/// General idea: for each possible pair of characters (L, R), the number of occurences of each character for iteration N of expanding
/// that pair is equal to the number of occurrences of each character for (L, C) for N - 1 plus the occurrences of each character for
/// (C, R) for N - 1, minus 1 occurrence of the character C (for the overlap). Using this relationship you can build up the number
/// of occurrences of each character for each pair of characters in the original template by just repeateadly adding results from
/// the previous iteration.
module SolveIncremental =

    // store the accumulated counts of each character after expanding a pair some number of times.
    // pair is the key, value is the accumulated result for that pair.
    type CountAccumulator = Map<char * char, BigCounter<char>>

    let incrementalCounter (rules : Map<char * char, char>) (totalIterations : int) =

        // for our known problem sets, the rules perfectly cover all 2-character combinations,
        // so we don't have to calculate the alphabet somehow
        let allCharacterPairs = Map.keys rules

        // start with a mapping where the pairs haven't been expanded at all; after 0 iterations
        // the number of characters for that pair only includes the 2 characters in the pair
        let initialAccumulator : CountAccumulator =
            allCharacterPairs
            |> Seq.map (fun pair ->
                let baseCount =
                    BigCounter.empty
                    |> BigCounter.incr (fst pair)
                    |> BigCounter.incr (snd pair)
                pair, baseCount
            )
            |> Map.ofSeq

        // here's our core iterative algorithm; on every iteration we build the next accumulator
        // by adding results from the previous one. The execution time is bounded by C * N * M 
        // where C is how long it takes to execute the function once, N is  totalIterations, 
        // and M is the number of possible character pairs; and the memory usage is bounded to 
        // 2 * sizeof(CountAccumulator). There is also no stack expansion because the only
        // recursive functions are tail-call optimized, which is something we could not do
        // with our previous solutions where the recurrence was not in tail call position.
        let rec generateNext (iteration : int) (current : CountAccumulator) =
            
            // a helper function to play nice with Seq.map + Map.ofSeq
            let nextForPair (pair : char * char) =
                let l, r = pair
                let c = Map.find (l, r) rules
                let leftPrevResult = Map.find (l, c) current
                let rightPrevResult = Map.find (c, r) current
                let nextResult =
                    BigCounter.add leftPrevResult rightPrevResult
                    |> BigCounter.decr c
                pair, nextResult
                
            if totalIterations - iteration = 0 then
                current
            else
                let next =
                    allCharacterPairs
                    |> Seq.map nextForPair
                    |> Map.ofSeq

                // perform the next iteration; this will be converted to a loop by TCO
                generateNext (iteration + 1) next

        // now THIS mapping contains the result of expanding every possible pair of characters up to `totalIterations`!
        let expandedCounts = 
            generateNext 0 initialAccumulator

        // At this point, totalling things up is the same as SolveRecursive.solveWith, so we just need to have a signature
        // that matches what that function expects and we can re-use it! We've already calculated a result for each possible
        // pair of characters, so our ExpansionFunction, instead of actually evaluating anything, can just look it up.

        fun (pair : char * char) ->
            expandedCounts[pair]


/// Part 1 is small enough that a literal approach is sufficient. 
/// After 10 rounds the string is very large but still managable.
module Part1 =

    let solve (template : char list, rules : Map<char * char, char>) =
        let chain = SolveLiteral.expandN rules 10 template
        let (_, most), (_, least) = SolveLiteral.countChars chain
        most - least
        |> sprintf "%d"

module Part2 =
    
    open System.Diagnostics

    let solve (template : char list, rules : Map<char * char, char>) =

        let time thunk =
            let timer = Stopwatch.StartNew()
            let result = thunk ()
            timer.Stop()
            result, timer.ElapsedMilliseconds
        
        let iters = 20

        let doSolver fn =
            SolveRecursive.solveWith
                fn
                rules
                iters
                template


        let doRecursive () =
            doSolver 
                SolveRecursive.recursiveCounter
                
            
        let doMemoized () =
            doSolver 
                SolveRecursive.memoizedCounter
                

        let doIterative () =
            doSolver 
                SolveIncremental.incrementalCounter

        for sol in [doRecursive; doMemoized; doIterative] do
            let result, ms = time sol
            printfn "%A ~ %d ms" result ms

        "0"
        // let least = List.minBy snd counts
        // let most = List.maxBy snd counts

        // printfn "Most: %A Least: %A" most least

        // ((snd most) - (snd least)).ToString()


[<Register.Solution(14, Description="Extended Polymerization")>]
let solve (input : IO.Stream) : Map<string, string> =
    match (parseInput input) with
    | Failure (msg, error, _) ->
        printfn "%s: %A" msg error
        Map.empty
    | Success (parsed, _, _) ->
        Solution.fromFunctions parsed [ Part1.solve ; Part2.solve ] 
    