[<adventlib.Register.Module>]
module advent.Day3

open System

/// a line of input and convert into an array of integers
let digitize (line : string) : int[] =
    let digitOf c = if c = '1' then 1 else 0
    line.ToCharArray() |> Array.map digitOf

type MostCommonDigit =
    | Zero
    | One
    | Neither

let invert (mcd : MostCommonDigit) : MostCommonDigit =
    match mcd with
    | Zero -> One
    | One -> Zero
    | Neither -> Neither

let toChar (tiebreaker : unit -> char) (mcd : MostCommonDigit) : char =
    match mcd with
    | Zero -> '0'
    | One -> '1'
    | Neither -> tiebreaker ()

let binaryStrToInt (bs : string) : int =
    Convert.ToInt32(bs, 2)

/// Find bit positions in the input where '1' is the most common digit
let mostCommonDigits (lines : string array) : MostCommonDigit array =
    
    // to determine the most common bit at each position, 
    // define a majority as more than half of the bits for any position:
    let numberOfInputs = Array.length lines
    
    let decide (numberOfOnes : int) : MostCommonDigit =
        let numberOfZeros = numberOfInputs - numberOfOnes
        match (compare numberOfOnes numberOfZeros) with
        | 1 -> One
        | -1 -> Zero
        | 0 -> Neither
        | x -> failwithf "What the actual hell? compare returned %d" x
    
    // convert each line to an array of ints, either 1 or 0
    lines
    |> Array.map digitize
    // then sum in columns e.g.:
    //   [ 1 0 1 ]
    // + [ 0 1 1 ]
    // = [ 1 1 2 ]
    |> Array.reduce (Array.map2 (+))
    // for each position, if the sum is greater than half the number of lines,
    // then 1 was the most common digit at that position
    |> Array.map decide


let part1 (input : string array) =

    let mostCommon = mostCommonDigits input
    let leastCommon = Array.map invert mostCommon
    
    let convertToChars = toChar (fun () -> failwith "this should never happen in part 1!")
    
    // gamma rate is composed of the most common digit at each position
    let gamma = mostCommon
                    |> Array.map convertToChars
                    |> String
                    |> binaryStrToInt

    // epsilon rate is composed of the least common digit at each position
    let epsilon = leastCommon
                        |> Array.map convertToChars
                        |> String
                        |> binaryStrToInt
    
    gamma * epsilon

type Criteria = string -> bool

// index, inputs -> predicate
type CriteriaBuilder = (int) -> (string array) -> Criteria

let part2 (input: string array) =

    let rec findRate (criteriaBuilder : CriteriaBuilder) (index : int) (values : string array) =
        let predicate = criteriaBuilder index values
        let remaining = Array.filter predicate values
        // printfn "%A" remaining
        match remaining with 
        | [| single |] -> single
        | [| |] -> failwith "this shouldn't happen in part 2"
        | _ -> findRate criteriaBuilder (index + 1) remaining

    // CirteriaBuilder
    let hasMostCommonAt (index : int) (values : string array) : Criteria =
        // technically this finds the most common bit for all positions, 
        // not only the most common bit for the current position,
        // and so is wildly inefficient. But function reuse damnit.
        let allMostCommon = mostCommonDigits values
        
        // the default is important; it implements the case where 0 and 1 were equally common!
        // whatever value it returns will count as a match
        let charConverter = toChar (fun () -> '1')
        let mostCommonHere = charConverter allMostCommon.[index]

        fun (value : string) ->
            value[index] = mostCommonHere

    // CriteriaBuilder
    let hasLeastCommonAt (index : int) (values : string array) : Criteria =
        let charConverter = toChar (fun () -> '0')
        let leastCommonHere = mostCommonDigits values
                                |> Array.map invert
                                |> (fun ms -> ms.[index])
                                |> charConverter

        fun (value : string) ->
            value[index] = leastCommonHere


    let oxygen = findRate hasMostCommonAt 0 input |> binaryStrToInt
    let carbon = findRate hasLeastCommonAt 0 input |> binaryStrToInt

    oxygen * carbon

open adventlib

[<Register.Solution(3, Description="Binary Diagnostic")>]
let fullSolution stream =
    let lines = stream |> Input.getLines |> List.toArray
    Solution.fromValues [
        part1 lines ;
        part2 lines ;
    ]
