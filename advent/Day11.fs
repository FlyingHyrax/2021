[<adventlib.Register.Module>]
module advent.Day11

open adventlib
open System

let PRINT = false

type Octopus =
    | Charging of level : int // 0..9
    | Flashed

module Octopus =
    let init level =
        Charging level

    let zero () =
        Charging 0
    
    let incr octo =
        match octo with
        | Flashed  -> Flashed
        | Charging level -> Charging (level + 1)

    let resetFlashed octo =
        match octo with
        | Flashed -> Charging 0
        | _ -> octo

    let toString octo =
        match octo with
        | Charging level -> level |> string
        | Flashed -> "!"
            

// grid is 10x10, so indexed 0..9
// we'll do [row, col] for index order

let parseLine (line : string) =
    line.ToCharArray ()
    |> Array.map (string >> Int32.Parse >> Octopus.init)

let parseInput (lines : string list) : Octopus[,] =
    lines
    |> List.map parseLine
    |> array2D

module Grid =
    
    /// the first (lowest) valid index for a row
    let firstRowIndex (grid : 'a[,]) =
        Array2D.base1 grid

    /// the last (highest) valid index for a row
    let lastRowIndex (grid : 'a[,]) =
        Array2D.length1 grid - 1

    /// the first (lowest) valid indedx for a column
    let firstColIndex (grid : 'a[,]) =
        Array2D.base2 grid
    
    /// the last (largest) valid index for a column
    let lastColIndex (grid : 'a[,]) =
        Array2D.length2 grid - 1

    let unboundedNeighborCoordinates (row : int) (col : int) =
        seq {
            for r in [row - 1 ..row + 1] do
                for c in [col - 1 .. col + 1] do
                    if not (r = row && c = col) then
                        yield (r, c)
        }

    let neighborCoordinates (grid : 'a[,]) (row : int) (col : int) =
        let fr = firstRowIndex grid
        let lr = lastRowIndex grid
        let fc = firstColIndex grid
        let lc = lastColIndex grid
        
        let inBounds (r, c) =
            fr <= r &&
            r <= lr &&
            fc <= c &&
            c <= lc

        unboundedNeighborCoordinates row col
        |> Seq.filter inBounds

module Sim =

    type State =
        { count : int
        ; grid : Octopus[,]
        }

    let startingState grid =
        { count = 0; grid = grid }

    let printState state =
        let toJagged (arr: 'a[,]) =
            [| for x in 0 .. Array2D.length1 arr - 1 do
                    yield [| for y in 0 .. Array2D.length2 arr - 1 -> arr.[x, y] |]
            |]

        let rowString (row : Octopus[]) =
            row
            |> Array.map Octopus.toString
            |> String.concat ""

        if PRINT then
            state.grid
            |> toJagged
            |> Array.map rowString
            |> String.concat "\n"
            |> printfn "%s"

        state


    // "First, the energy level of each octopus increases by 1"
    let incrementAll (state : State) =
        { state with grid = Array2D.map Octopus.incr state.grid }
        
    // "Then, any octopus with an energy level greater than 9 flashes"
    let doFlashes (state : State) =
        
        // there's probably a cleaner wat, but this seems 
        // easiest since the 2D array is already mutable
        let mutable count = state.count
        let grid = state.grid

        /// Check if the octopus in a given position has charged and should flash.
        /// If so, update the state of that octopus, then increment the energy level
        /// of all its neighbors. Otherwise, do nothing.
        let rec checkFlash (row : int) (col : int) =
            match grid[row, col] with
            | Flashed -> 
                // already flashed - can't flash again
                ()
            | Charging level ->
                if level > 9 then
                    // flash, then...
                    Array2D.set grid row col Flashed
                    count <- count + 1
                    // ..increment all neighbors
                    Grid.neighborCoordinates grid row col
                    |> Seq.iter chargeNeighbor

        /// Increase the charge level of an octopus at a given position based
        /// on one of its neighbors flashing. After increasing the charge level,
        /// check if this causes the octopus itself to flash, triggering recursion.
        and chargeNeighbor (row : int, col : int) =
            grid[row, col] <- Octopus.incr grid[row, col]
            checkFlash row col

        Array2D.iteri (fun r c _ -> checkFlash r c) grid

        { count = count ; grid = grid }


    // "Finally, any octopus that flashed during this step has its energy level reset to 0"
    let resetFlashed (state : State) =
        { state with grid = Array2D.map Octopus.resetFlashed state.grid }
        
    let step =
        incrementAll
        >> doFlashes
        >> printState
        >> resetFlashed
        

let part1 (startingGrid : Octopus[,]) (iterations : int) =
    let rec loop count state =
        match count with
        | 0 -> state
        | _ -> 
            if PRINT then
                printfn "\n%d" (iterations - count)
            loop (count - 1) (Sim.step state)

    let initialState = Sim.startingState startingGrid
    let finalState = loop iterations initialState
    finalState.count

let part2 (startingGrid : Octopus[,]) =
    // THIS RECURSIVE FUNCTION HAS NO BASE CASE!
    // The problem statement says our test will eventually succeed, 
    // so let the runtime catch us if we blow up the stack.
    let rec loop (stepNumber : int) (state : Sim.State) =
        
        // execute one step
        let state' = Sim.step state
        
        // Compare the total number of flashes from before the step to the 
        // number of flashes after. There are 100 octopuses, so if the flash
        // count increases by 100 then all of them flashed in the same step.
        if (state'.count - state.count) = 100 then
            stepNumber
        else
            loop (stepNumber + 1) state'

    Sim.startingState startingGrid
    |> loop 1
    

[<Register.Solution(11, Description="Dumbo Octopus")>]
let solve (input : IO.Stream) : Map<string, string> =
    let startingGrid = input |> Input.getLines |> parseInput
    let p1 = part1 (Array2D.copy startingGrid) 100
    let p2 = part2 (Array2D.copy startingGrid)
    Map.ofList [
        ("Part 1", string p1)
        ("Part 1", string p2)
    ]
