[<adventlib.Register.Module>]
module advent.Day2

open adventlib

type Direction =
    | Forward
    | Up
    | Down

type Movement = 
    { direction : Direction
    ; magnitude : int 
    }

let movement (d: Direction) (m: int) : Movement =
    { direction = d; magnitude = m; }

let parse (ln : int) (line : string) : Result<Movement, string> =
    let split (line : string) =
        match line.Split(' ') with
        | [| ds; ms; |] -> Ok (ds, ms)
        | _ -> Error $"Line {ln}: Wrong number of tokens in line: {line}"

    let parseDirection (token : string) =
        match token.ToLower() with
        | "forward" -> Ok Forward
        | "up" -> Ok Up
        | "down" -> Ok Down
        | _ -> Error $"Line {ln}: Not a valid direction: {token}"

    let parseInt (token : string) =
        try
            Ok (int token)
        with
        | ex -> Error $"Line {ln}: Can't convert '{token}' to integer: {ex.Message}"

    Result.computation {
        let! (ds, ms) = split line
        let! d = parseDirection ds
        let! m = parseInt ms
        return movement d m
    }

    

module Part1 =
    type Position =
        { lateral : int
        ; depth : int
        }  
        
    let start: Position = { lateral = 0; depth = 0 }

    let hash pos = pos.depth * pos.lateral

    let move (pos : Position) (mv : Movement) : Position =
        match mv.direction with
        | Forward -> { pos with lateral = pos.lateral + mv.magnitude }
        | Up -> { pos with depth = pos.depth - mv.magnitude }
        | Down -> { pos with depth = pos.depth + mv.magnitude }

module Part2 =
    type State =
        { horizontal : int
        ; depth : int
        ; aim : int
        }
    
    let start: State = { horizontal = 0; depth = 0; aim = 0 }
    
    let hash state = state.horizontal * state.depth
    
    let move (st : State) (mv : Movement) : State =
        match mv.direction with
            | Up -> { st with aim = st.aim - mv.magnitude }
            | Down -> { st with aim = st.aim + mv.magnitude }
            | Forward -> { st with
                            horizontal = st.horizontal + mv.magnitude ;
                            depth = st.depth + (st.aim * mv.magnitude) ; 
                            }
            
// both solutions have exactly the same structure, only the state type and folder functions differ
let solve start mover finalizer input =
    let instructions = Array.mapi parse input
                        |> Array.toList
                        |> Result.sequenceM

    match instructions with
    | Error err -> err
    | Ok steps -> 
        steps 
        |> Seq.toList 
        |> List.fold mover start
        |> finalizer
        |> string

[<Register.Solution(2, Description="Dive!")>]
let fullSolution (input : System.IO.Stream) =
    let lines = input |> Input.getLines |> List.toArray
    Solution.fromFunctions lines [
        (solve Part1.start Part1.move Part1.hash) ;
        (solve Part2.start Part2.move Part2.hash) ;
    ]
