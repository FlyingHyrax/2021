namespace advent

open adventlib
open FParsec
open System

module Trace =
    let ON = true

    let console (s : string) =
        if ON then
            Console.WriteLine(s) |> ignore
    
    let put format =
        Printf.ksprintf console format

    let putTable header fmtRow table =
        let rows = Map.toList table
                    |> List.map fmtRow
                    |> String.concat "\n"
        put "%s:\n%s" header rows

    let putAssignments (bound : Map<'a, 'b>) =
        let line (k, v) =
            sprintf "%s = %s" (string k) (string v)

        putTable "Bindings" line bound

    let putPossibilities (poss : Map<'a, Set<'b>>) =
        let line (k, v) =
            sprintf "%s: %A" (string k) v
        
        putTable "Unknown" line poss

/// Domain models
module SevenSegmentDisplay =
    
    /// Represent individual segments in a 7-segment display by location
    type Segment = TT | TL | TR | MM | BL | BR | BB

    /// Digits that can be displayed on a single 7-segment display
    type Digit = D0 | D1 | D2 | D3 | D4 | D5 | D6 | D7 | D8 | D9
        

    module Digit =
        /// All digits in a list in order from smallest to largest
        let all = [D0 ; D1 ; D2 ; D3 ; D4 ; D5 ; D6 ; D7 ; D8 ; D9]

        /// Numeric value of a digit        
        let intValue digit =
            match digit with
            | D0 -> 0
            | D1 -> 1
            | D2 -> 2
            | D3 -> 3
            | D4 -> 4
            | D5 -> 5
            | D6 -> 6
            | D7 -> 7
            | D8 -> 8
            | D9 -> 9

        /// The segments used to display a digit
        let segments digit =
            match digit with
            | D0 -> [| TT; TL; TR;     BL; BR; BB |]
            | D1 -> [|         TR;         BR     |] 
            | D2 -> [| TT;     TR; MM; BL;     BB |]
            | D3 -> [| TT;     TR; MM;     BR; BB |]
            | D4 -> [|     TL; TR; MM;     BR     |]
            | D5 -> [| TT; TL;     MM;     BR; BB |]
            | D6 -> [| TT; TL;     MM; BL; BR; BB |]
            | D7 -> [| TT;     TR;         BR     |]
            | D8 -> [| TT; TL; TR; MM; BL; BR; BB |]
            | D9 -> [| TT; TL; TR; MM;     BR; BB |]
            |> Set.ofArray

        (* We could use a match statement, but for ease and variety we'll pre-compute this table for 'withSegmentCount' *)
        let _digitsByNumberOfSegments = 
            all |> List.groupBy (segments >> Set.count) |> Map.ofList

        /// <summary>
        /// Given a number of segments, return a list of digits that are displayed 
        /// using exactly that number of segments.
        /// </summary>
        /// <param name="n">Number of segments</param>
        /// <returns>Digits that include that number of segments</returns>
        let withSegmentCount n =
            _digitsByNumberOfSegments
            |> Map.tryFind n
            |> Option.defaultValue List.empty

/// Extensions for Sets
module Set =
    /// <summary>
    /// Test if a set contains exactly one element.
    /// </summary>
    /// <param name="s">set to test</param>
    /// <typeparam name="'a">type of set elements</typeparam>
    /// <returns>true if the set contains one element</returns>
    let isSingleton s =
        (Set.count s) = 1
    
    /// <summary>
    /// Extract a single element from a set.
    /// This is a meaningless operation unless the set satisfies 'isSingleton'.
    /// This function does not verify that the set only contains one value and 
    /// will fail if the set is empty or return an arbitrary value if the set
    /// contains more than one item.
    /// </summary>
    /// <param name="s">singleton set</param>
    /// <typeparam name="'a">type of set elements</typeparam>
    /// <returns>The value contained in the singleton set</returns>
    let getSingletonValue s =
        Set.toList s |> List.head

/// Extensions for Seq
module Seq =
    /// Consume elements from a source sequence, updating an accumulator with each new element,
    /// until a test condition is met. Returns the final result and discards any remaining elements
    /// from the source sequence. This should be similar to 'fold', but:
    /// (1) can return early, when the test function succeeds
    /// (2) will fail if the source sequence is exhausted
    let doUntil test update state source =
        let rec loop acc xs =
            if test acc then
                acc
            else
                let acc' = update acc (Seq.head xs)
                loop acc' (Seq.tail xs)
        loop state source

/// Implementation of our assignment-based constraint propagation strategy.
/// Given a set of variables and a set of possible values for each variable,
/// this implements the constraint that every variable is assigned exactly one
/// value, and each value can only be assigned to exactly one variable.
module SingleSpaceUniqueAssignment =
    type Domain<'k> when 'k : comparison = Domain of Set<'k>
    type Range<'v> when 'v : comparison = Range of Set<'v>

    type Context<'k, 'v>
        when 'k : comparison and 'v : comparison =
            { assignments : Map<'k, 'v>
            ; unknowns : Map<'k, Set<'v>>
            }

    module Context =
        let totalVars ctx =
            Map.count ctx.assignments + Map.count ctx.unknowns

        let isSolved expected ctx =
            Map.count ctx.assignments = expected &&
            Map.isEmpty ctx.unknowns

    let rec assignSingletons (ctx: Context<'k, 'v>) =
        let singlePossibleValue, multiplePossibleValues = 
            Map.partition (fun _ vs -> Set.isSingleton vs) ctx.unknowns

        if Map.isEmpty singlePossibleValue then
            // BASE CASE
            ctx
        else
            // RECURSIVE CASE
            singlePossibleValue
            // 1) convert sets of one value into just that value
            |> Map.map (fun _ vs -> Set.getSingletonValue vs)
            // 2) move keys with one value from the 'free' table to the 'bound' table
            |> Map.fold (fun newCtxInProgress key value -> 
                // 2.a) add the new key to 'bound'
                let assignments' = Map.add key value newCtxInProgress.assignments
                // 2.b) remove the value as a possibility for any key we still don't know the value for
                let unknowns' = Map.map (fun _ possibleValues -> Set.remove value possibleValues) newCtxInProgress.unknowns
                { assignments = assignments' ; unknowns = unknowns' }
            ) { ctx with unknowns = multiplePossibleValues }
            // 3) re-check the table for new keys that now have only one value
            |> assignSingletons

    // FIXME: this is almost a stand-in for assignment, EXCEPT that it doesn't move keys from the unknown table to the assigned table!
    let rec constrainByDisjointSubsets (ctx: Context<'k, 'v>) =
        // if there is already a set of keys, add this key to it
        // otherwise create a new set containging the key
        let addOrCreate (k: 'k) (ks : Set<'k> option) =
            match ks with
            | Some existing -> Set.add k existing
            | None -> Set.singleton k
            |> Some

        // flip the map! go from a mapping from key -> possible values, to
        // possible values -> keys, to answer whether any keys have the *same*
        // set of possible values, and if so which ones and how many.
        let possibleValuesToKeys =
            ctx.unknowns
            |> Map.fold (fun mm k vs ->
                Map.change vs (addOrCreate k) mm) Map.empty

        // identify cases where the set of possible values is the *same size* as the set of keys sharing those possible values.
        // i.e. pairs or triples of keys that have the same pair or triple of possible values
        let disjointSets =
            possibleValuesToKeys
            |> Map.filter (fun vs ks -> Set.count vs = Set.count ks)

        // break that down into keys that are part of any disjoint set
        let keysInDisjointSets =
            disjointSets
            |> Map.values 
            |> Set.unionMany
        
        let valuesInDisjointSets =
            disjointSets
            |> Map.keys
            |> Set.unionMany

        // IF a key appears in the above, then it is part of a disjoint set, and the values are the values in that set,
        // any key NOT in the above, can remove from its possible values any value that IS in the above

        let updatedUnknowns = 
            Map.map (fun aKey itsPossibleValues -> 
                if Set.contains aKey keysInDisjointSets then
                    itsPossibleValues
                else
                    Set.difference itsPossibleValues valuesInDisjointSets
                ) ctx.unknowns
        
        { ctx with unknowns = updatedUnknowns }



module UniqueAssignmentConstraint =
    /// Tracks 2 groups of identifiers: those that we know the exact value of ("bound"), and those we don't ("free").
    /// For bound identifiers, store the value bound to the identifier.
    /// For free identifiers, store a set of possible values.
    type Env<'a, 'v> 
        when 'a : comparison and 'v : comparison =
        { bound : Map<'a, 'v>
        ; free : Map<'a, Set<'v>>
        }

    /// Our strategy constrains the possible values for identifiers based on assignment of known values,
    /// but doesn't otherwise specify how to reduce the number of possible values for an identifier.
    /// Callers must provide their own function to reduce the possible values somehow. This function
    /// takes in a map of "free" identifiers (those with more than one possibility) and an arbitrary piece
    /// of additional data. The function should use the data to try to reduce the number of possible values
    /// for one or more identifiers, and return the modified version of the input map.
    /// TL;DR: Free Vars -> New Info -> Constrained Vars
    type ConstrainFn<'a, 'v, 't>
        when 'a : comparison and 'v : comparison =
            Map<'a, Set<'v>> -> 't -> Map<'a, Set<'v>>
    
    /// A problem instance is "solved" if all identifiers have been assigned a value and
    /// there are no remaining identifiers with more than one possible value. This function
    /// takes a starting environment and uses it to determine how many identifiers need to be 
    /// assigned.
    let defineIsSolved initialEnv =
        // assume the number of variables we need to assign a value for is
        // the initial number of unassigned/free + the initial number of assigned/bound
        let totalVarCount = Map.count initialEnv.free + Map.count initialEnv.bound
        fun currentEnv ->
            // an env is "solved" if every variable is assigned a single value,
            // leaving no variables in the unassigned/free table
            Map.count currentEnv.bound = totalVarCount &&
            Map.isEmpty currentEnv.free

    let rec assignSingletons (env : Env<'a, 'v>) =
        // Traverse our table to find keys that have only one possible value remaining.
        // We'll call keys with a set of only one possible value "singletons".
        let singletons, remaining =
            Map.partition (fun _ possibleValues -> Set.isSingleton possibleValues) env.free
        
        if Map.isEmpty singletons then
            // BASE CASE 
            // Any key that we know the exact value for has already been moved from the 
            // 'free' table to the 'bound' table, so we do not need to modify anything.
            env
        else
            // RECURSIVE CASE
            singletons
            // 1) convert sets of one value into just that value
            |> Map.map (fun _ vs -> Set.getSingletonValue vs)
            // 2) move keys with one value from the 'free' table to the 'bound' table
            |> Map.fold (fun env' var value -> 
                // 2.a) add the new key to 'bound'
                let bound' = Map.add var value env'.bound
                // 2.b) remove the value as a possibility for any key we still don't know the value for
                let free' = Map.map (fun _ possibleValues -> Set.remove value possibleValues) env'.free
                { bound = bound' ; free = free' }
            ) { env with free = remaining }
            // 3) re-check the table for new keys that now have only one value
            |> assignSingletons

    let applyConstraint (initial : Env<'a, 'v>) (constrain : ConstrainFn<'a, 'v, 't>) (inputs : 't list) =
        let isSolved = defineIsSolved initial
        
        let finalEnv = Seq.doUntil isSolved 
                            (fun env input -> 
                                // for each new input, constrain the possible values of unbound variables,
                                // then check for any that are left with only one possibility
                                assignSingletons {env with free = constrain env.free input})
                            initial inputs
        finalEnv.bound  // woot


[<Register.Module>]
module Day8_Redux =
    open SevenSegmentDisplay


    /// Encapsulate the character patterns we're given in a type
    type Pattern = Pattern of Set<char>

    module Pattern =
        let map f (Pattern s) =
            f s
        
        let ofString (s : string) =
            Pattern (Set.ofSeq s)

        let toString (Pattern s) =
            s |> Set.toArray |> Array.sort |> String


    let formatPossibilityTable unboundSegments =
        Map.toList unboundSegments
        |> List.map (fun (s, cs) -> sprintf "%s: %A" (string s) cs)
        |> String.concat "\n"




    /// Apply our key constraint: 
    /// - There are a limited number of digits that use the same number of segments as a given pattern.
    /// - Those digits use some subset of display segments in common (e.g., '1' and '4' both use TR and BR)
    /// - Those segments could only be represented by a character from that pattern!
    let constrainWithPattern (segmentsToPossibilies : Map<Segment, Set<char>>) (pattern : Set<char>) =
        
        // The pattern is made up of some set of characters and each character corresponds to some display segment.
        // Get the digits that use the same number of segments as the number of characters in the pattern:
        let possibleDigits = Digit.withSegmentCount (Set.count pattern)
        Trace.put "Possible digits: %A" possibleDigits
        
        // Each digit is represented by a unique set of display segments, but those sets overlap.
        // Find any segments that are used in every digit from above.
        let segmentsInCommon = possibleDigits
                            |> List.map Digit.segments
                            |> Set.intersectMany
        Trace.put "Segments in common: %A" segmentsInCommon

        // Now update our table of possible values; for every segment ('seg') and its set of possible values ('poss')
        segmentsToPossibilies |> Map.map (fun seg poss ->
            // Is this segment one of the ones used by every digit that could possibly be represented by the pattern?
            if Set.contains seg segmentsInCommon then
                // If so, the only characters that might correspond to this segment are the ones from the pattern.
                // Set intersection removes any possibilities that aren't in the pattern set.
                Set.intersect poss pattern
            else
                // Otherwise, don't change the possible values for the segment - this pattern doesn't constrain its possibilities.
                poss
        )

    /// Constraint propagation:
    /// - Consider each segment part of one of two groups: "assigned" or "unbound" (a.k.a. unassigned)
    ///     - Segments that we *know* (have deduced) the representative character for are "assigned"
    ///     - Segments that we *don't* know the exact character for are "unassigned"
    /// - Maintain a table of assigned segments that maps a segment to the character that represents it.
    /// - Maintain a table of unbound/unassigned segments that maps a segment to the possible characters that *could* represent it.
    /// 
    /// This function searches the "unbound" table for display segments that have only one possible value.
    /// 
    /// If a segment has only one possible value, then we've deduced the character that corresponds to that segment.
    /// 1. Add the segment to the 'assigned' group, and remove it from the 'unbound' group.
    /// 2. Remove the character as a possible value for any segment still in the 'unbound' group
    let rec assignSingletons (assignments : Map<Segment, char>) (unbound : Map<Segment, Set<char>>) =
        
        (*
            Split our table of unassigned segments into 2 sub-groups:
            Unassigned segments with one possible value remaining, and
            unassigned segments with multiple possible values remaining.
        *)
        let unboundWithOnePossibility, remainingUnbound = 
            Map.partition (fun _ possibles -> 1 = Set.count possibles) unbound

        (*
            Now check if we have any segments with only one possible value.
            Any segment that we've already done this for will have already been removed from the 'unbound' table
            and won't appear in the partitions again, so this only finds segments with one possible value that
            *were not already assigned* previously.
        *)
        if Map.isEmpty unboundWithOnePossibility then
            (* 
                In this case, we've already assigned all the segments we know the character for; 
                we haven't deduced any new ones and don't need to manipulate the tables at all. 
            *)
            assignments, unbound
        else
            (*
                Otherwise, we found *at least one* segment that only has one possible value, but hasn't been assigned yet.
                Process each new deduction to create updated tables: a new table of assigned segments, and
                a new table of unassigned segments mapped to their remaining possible values.
            *)
            let updatedAssignments, updatedUnbound =
                
                (* Using 'fold' lets us consume the rows in the table in a way that transforms them into a different shape. *)
                Map.fold (fun (assignedSoFar, unboundSoFar) segmentToAssign singletonSet -> 
                    (*
                        Notes on 'folder' function arguments:
                        - For our fold 'state', we'll use a tuple of our assignments table and unbound table together.
                          At each step, we can change these tables as needed and return updated ones. As we process, 
                          each step will be given the updated tables produced in the previous step. The final tables 
                          are whatever is returned by the last step.
                        - Because of the shape/type of the table we're iterating over (unboundWithOnePossibility), we
                          know the second two arguments will be a segment that we've deduced the correct character for,
                          and a set containing only the character representing that segment.
                    *)
                    
                    (* 
                        Elements in sets don't have an order - we can't say "give me the first element in the set". 
                        But we know logically that this set only has one element, so we can get that element by
                        turning it into a list, and asking *that list* for its first element.
                    *)
                    let charToAssign = Set.toList singletonSet |> List.head
                    Trace.put "%s = %c" (string segmentToAssign) charToAssign
                    
                    (* First, add the segment to our assignment table, with its corresponding character for its value *)
                    let updAssigned = Map.add segmentToAssign charToAssign assignedSoFar
                    
                    (* 
                        Next, update the possible values for the remaining segments we still don't know the character for.
                        The character we just assigned can only be assigned once, so it can't be a possibility for any
                        other segment. Change the 'unbound' table by removing the character from the set of possible values
                        for every segment still in that table.
                    *)
                    let updUnbound = Map.map (fun _ possibles -> Set.remove charToAssign possibles) unboundSoFar

                    (* Lastly, bundle the two updated tables into a tuple for the next iteration *)
                    Trace.putPossibilities updUnbound
                    updAssigned, updUnbound
                    ) // end 'folder'
                    (* 
                        Starting state; this tuple is passed as the first argument to the very first iteration of the
                        'folder' function above. Be sure to use 'remainingUnbound', the second group from our initial
                        partition, since that's the table of display segments that still have multiple possibilities.
                    *) 
                    (assignments, remainingUnbound)
                    (* 
                        Table to 'iterate' over - key/value pairs from this table are passed one at a time as the
                        second and third arguments to the folder, respectively. These are segments from the partition
                        that we've discovered have only one possible value.
                    *)
                    unboundWithOnePossibility 
            
            (* 
                Now that we have updated tables, check them again! Since when we assign a character to a segment it
                can  *remove possible values* for other segments, we may have just deduced the correct character for
                one or more additional segments!
            *)
            assignSingletons updatedAssignments updatedUnbound


    let decode (examples : Pattern list) =
        // Our starting table of possible values for each display segment.
        // Before constraining anything, any segment could be any character.
        let segmentsToPossibilities =
            [ TT ; TL ; TR ; MM ; BL; BR ; BB ]
            |> List.map (fun s -> (s, Set.ofList ['a'..'g']))
            |> Map.ofList

        let isSolved assigned unknown =
            7 = Map.count assigned &&
            0 = Map.count unknown

        let rec loop assigned unknown patterns =
            if isSolved assigned unknown then
                assigned
            else
                match patterns with
                | (Pattern cs)::rest ->
                    Trace.put "Using pattern: %A" cs
                    
                    let moreConstrainedUnknown = constrainWithPattern unknown cs
                    Trace.putPossibilities moreConstrainedUnknown
                    
                    let updatedAssigned, updatedUnknown = assignSingletons assigned moreConstrainedUnknown
                    Trace.putAssignments updatedAssigned
                    
                    loop updatedAssigned updatedUnknown rest
                | _ -> 
                    failwith "Ran out of patterns without solving the assignments!"
        
        loop Map.empty segmentsToPossibilities examples




    /// Encapsulate a single line of input
    type Entry =
        { examples : Pattern list
        ; display : Pattern list
        }

    module Entry =

        /// A parser for a single line of problem input
        let parser : Parser<Entry, unit> =
            let letter = anyOf ['a'..'g']
            let pattern = many1Chars letter |>> Pattern.ofString
            let spc = pchar ' '
            // left side of input line
            let observedPatterns = many1 (pattern .>> spc)
            // right side of input line
            let displayedPatterns = sepBy1 pattern spc
            // complete input line with "|" separator
            (observedPatterns .>> pstring "| ") .>>. displayedPatterns
            |>> fun (l, r) -> { examples = l ; display = r }

        let solve entry =
            Trace.console "========================================================"
            Trace.put "START %A" entry.examples
            let characterForSegment = decode entry.examples

            let stringForDigit (d : Digit) =
                Digit.segments d
                |> Set.map (fun s -> characterForSegment[s])
                |> Set.toArray
                |> Array.sort
                |> String
            
            let digitForStr = Digit.all
                            |> List.map (fun d -> (stringForDigit d, d))
                            |> Map.ofList

            let digitValues = entry.display
                            |> List.map Pattern.toString
                            |> List.map (fun str -> digitForStr[str])
                            |> List.map Digit.intValue

            Trace.put "Display: %A" digitValues
            digitValues
            |> List.map (sprintf "%d")
            |> String.concat ""
            |> int
        
    /// Apply the parser to our input stream for as many lines of input as we have
    let parseInputStream (stream: IO.Stream) =
        let parseLines = many1 (Entry.parser .>> newline)
        let result = runParserOnStream parseLines () "" stream Text.Encoding.UTF8
        match result with
        | Success (values, _, _) -> values
        | Failure (_, err, _) -> failwith (err.ToString())

            
    [<Register.Solution(108, Description="Seven Segment Search - redux")>]
    let solvePart2 stream =
        let p2 = parseInputStream stream
                |> List.map Entry.solve
                |> List.sum
        Map.ofList [ ("Part 2", (string p2)) ]
