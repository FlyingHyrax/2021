[<adventlib.Register.Module>]
module advent.Day7

let isEven n =
    n % 2 = 0

let median xs =
    let l = Array.length xs
    if isEven l then
        // int division truncates toward zero, so we don't have
        // to use floor/ceil functions, we get floor from the division.
        // The index just above the midpoint; for the base case of l = 2
        // we get 2 / 2 = 1 
        let i2 = l / 2
        // The index just below the midpoint; for l = 2 we get
        // (2 / 2) - 1 = 0
        let i1 = i2 - 1
        // Get the values at those indices
        let x1 = xs.[i1]
        let x2 = xs.[i2]
        // Calculate a value halfway between the 2 actual values,
        // letting int division truncate down
        x1 + ((x2 - x1) / 2)
    else
        // integer division truncates toward 0, so for the base case of l = 1
        // we get 1/2 = 0 which is expected.
        let i = l / 2
        // since there are an odd number of values, there is an exact median
        // and we don't need to interpolate at all.
        xs.[i]


let part1 (input : string array) =
    
    // read the input positions
    let positions = Array.head input
                    |> fun l -> l.Split(",")
                    |> Array.map int
                    |> Array.sort

    // the least cost position to move all the inputs is the median -
    // positions above the median shift cost from the higher inputs to
    // the lower inputs, positions below the median do the opposite?
    let bestPosition = median positions

    // cost function is just the distance between the start and end positions
    let cost fromPos toPos =
        abs (fromPos - toPos)

    // total cost is the cost of moving each input from its original position to our assumed "best" position
    Array.sumBy (fun p -> cost p bestPosition) positions


(*
    For part 2, the movement cost is no longer linear, meaning the median
    is no longer the least-cost position. Outliers will have an outsized effect
    on total fuel cost.
*)
let part2 (input : string array) =
    let positions = Array.head input 
                    |> fun l -> l.Split(",")
                    |> Array.map int
                    |> Array.sort
    
    // new cost function, "sum of i for i=1 to n" where n is the linear distance
    let cost fromPos toPos =
        let dist = abs (fromPos - toPos)
        // formula for the arithemtic series
        // int division ok b/c the numerator will always be even!
        ((1 + dist) * dist) / 2

    (*
        Because our cost function is constant-time, the brute force solution is still tractable!
        The input is 1000 numbers with a max value of 1937 - round up to 2000 for napkin math.
        We know that the optimum position is at least in the bounds of the input positions, i.e.
        it will be between the minimum and maximum initial positions, because any position outside
        that range would have to have a worse cost.

        This makes the number of possible options ~2000, and we need to calculate 1000*2000 costs.
        So the brute force time complexity is O(n*m) where n is the number of inputs and m is the
        maximum value from the inputs. 2 million computations is cheap.
    *)

    let lowestPosition = Array.min positions
    let highestPosition = Array.max positions

    // calculate the total cost for each possible position, 
    // then keep the minimum
    seq {
        for candidate in lowestPosition .. highestPosition ->
            Array.sumBy (fun pos -> cost pos candidate) positions
    }
    |> Seq.min

open adventlib

[<Register.Solution(7, Description="The Treachery of Whales")>]
let fullSolution stream =
    let lines = stream |> Input.getLines |> List.toArray
    Solution.fromFunctions lines [ part1 ; part2 ]
