[<adventlib.Register.Module>]
module advent.Day4

open System

// for each square, store the number and true/false for marked/unmarked
type Square = int * bool

module Square =
    /// pretty print a square, using the ansi escape code for 'reversed' formatting
    /// for marked squares so the grid stays even - but this won't look good in a file!
    let pretty ((num, marked) : Square) : string =
        if marked then
            sprintf "\x1B[7m%2d\x1B[27m" num
        else
            sprintf "%2d" num

    let mark ((num, _) : Square) : Square =
        Square (num, true)

    let isMarked ((_, marked) : Square) : bool =
        marked


// for boards, store a 2D array of squares
// matching F# convention, dimension 1 = which row, dimension 2 = which column, 
// so board[x][y] => square at row x, column y
type Board = Square[,]

module Board =
    open Printf
    open System.Text
    
    /// pretty print a board, using the pretty-print functions for Squares
    /// and a StringBuilder
    let pretty (board : Board) =
        let buffer = new StringBuilder()
        let lastCol = (Array2D.length2 board) - 1

        let printer (_ : int) (col : int) (sq : Square) : unit =
            // stick a separator between cells
            if col <> 0 then
                bprintf buffer " "

            // print the cell
            sq |> Square.pretty |> bprintf buffer "%s"

            // if this is the end of a row, add a newline
            if col = lastCol then
                bprintf buffer "\n"

        board |> Array2D.iteri printer
        buffer.ToString()

    /// If the given number is on the given board, mark that square and return the modified board
    let mark (numToMark: int) (board: Board) : Board =
        let maybeMarkSq (sq : Square) =
            if (fst sq) = numToMark then
                Square.mark sq
            else
                sq

        Array2D.map maybeMarkSq board

    /// Return the rows of the board as a sequence of arrays of squares,
    /// where each array is the squares in a row left to right
    let rows (board : Board) : seq<Square[]> =
        let iLastRow = (Array2D.length1 board) - 1
        seq { for ri in 0 .. iLastRow -> board[ri, *] }

    /// Return the columns of the board as a sequence of arrays of squares,
    /// where each array is the squares in a column top to bottom.
    let cols (board : Board) : seq<Square[]> =
        let iLastCol = (Array2D.length2 board) - 1
        seq { for ci in 0 .. iLastCol -> board[*, ci] }
    
    /// Check if a board is currently a winner by seeing if any of its rows or columns are completely marked
    let isBingo (board : Board) : bool =
        rows board
        |> Seq.append (cols board)
        |> Seq.map (Array.forall Square.isMarked)
        |> Seq.exists id

    let score (board : Board) : int =
        board
        |> Seq.cast<Square>
        |> Seq.sumBy (fun (v, m) -> if m then 0 else v)
                

/// When we find a winning board, we also need the last number that was called, so keep them together
type Winner = Board * int


(* READING INPUT *)

// read the first line into an array of integers,
// returning the numbers + remaining lines of input
let readDrawList (lines : string seq) =
    
    let numbers = lines
                |> Seq.head
                |> fun line -> line.Split(',')
                |> Array.map int
                |> Array.toList

    let restOfTheInput = lines |> Seq.tail |> Seq.skip 1

    numbers, restOfTheInput

/// Read a board - does not handle unexpected lack of input, so your seq better have 5 lines to consume
let readBoard (lines : string seq) =
    let readRow (line : string) : int seq =
        let splitOpts = StringSplitOptions.RemoveEmptyEntries ^^^ StringSplitOptions.TrimEntries
        line.Split(' ', splitOpts)
        |> Array.toSeq
        |> Seq.map int

    let mutable remainingLines = lines
    let rows = seq {
        for _ in 1..5 do
            let row = readRow (Seq.head remainingLines)
            remainingLines <- Seq.tail remainingLines
            yield row
    }

    // the 'array2D' operator handily turns seq<seq<'T>> into 'T[,] for us
    let board = array2D rows
                |> Array2D.map (fun n -> Square (n, false))
    board, remainingLines

/// Read boards until we run out of input
let rec readBoards (lines: string seq) =
    seq {
        if Seq.isEmpty lines then
            yield! Seq.empty
        else
            let board, leftoverLines = readBoard lines
            yield board
            yield! readBoards (Seq.skipWhile String.IsNullOrWhiteSpace leftoverLines)
    }


(* PLAYING THE GAME *)

let rec markUntilFirstWinner (boards : Board seq) (calls : int list) =
    match calls with
    | [] -> None
    | first::rest ->
        let newBoards = Seq.map (Board.mark first) boards
        let winner = Seq.tryFind Board.isBingo newBoards
        match winner with
        | Some winningBoard -> Some (winningBoard, first)
        | None -> markUntilFirstWinner newBoards rest
    

let rec markUntilLastWinner (boardsInPlay : Board list) (calls : int list) (lastWinner : Winner option) =
    let playTheRound () =
        match calls with
        | [] -> lastWinner
        | calledNumber::numbersToCall ->
            let (newWinners, notWinners) = boardsInPlay   
                                        |> List.map (Board.mark calledNumber)
                                        |> List.partition Board.isBingo
            let mostRecentWinner =  if List.isEmpty newWinners then
                                        lastWinner
                                    else
                                        let newWinningBoard = List.head newWinners
                                        Some (newWinningBoard, calledNumber)

            markUntilLastWinner notWinners numbersToCall mostRecentWinner

    if List.isEmpty boardsInPlay then
        lastWinner
    else
        playTheRound ()


let part1 (input : string array) =
    let calls, remainingLines = readDrawList input
    
    printfn "%d calls" (List.length calls)
    
    let boards = remainingLines
                |> Seq.skipWhile String.IsNullOrWhiteSpace
                |> readBoards
                |> Seq.toList
    
    printfn "%d boards" (List.length boards)

    let doWeHaveAWinner = markUntilFirstWinner boards calls

    match doWeHaveAWinner with
    | None -> failwith "...there should be a winner"
    | Some (winningBoard, lastCall) ->
        printfn "First winner:\n%s" (Board.pretty winningBoard)
        printfn "With last called number %d" lastCall
        lastCall * Board.score winningBoard
    

let part2 (input : string array) = 
    let calls, remainingLines = readDrawList input
    let boards = remainingLines
                |> Seq.skipWhile String.IsNullOrWhiteSpace
                |> readBoards
                |> Seq.toList

    match markUntilLastWinner boards calls None with
    | None -> failwith "...there *really* should be a winner!"
    | Some (winningBoard, winningCall) -> 
        printfn "Last winner:\n%s" (Board.pretty winningBoard)
        printfn "With last called number %d" winningCall
        winningCall * Board.score winningBoard


open adventlib

[<Register.Solution(4, Description="Giant Squid (bingo)")>]
let fullSolution stream = 
    let lines = stream |> Input.getLines |> List.toArray
    Solution.fromFunctions lines [ part1 ; part2 ]
