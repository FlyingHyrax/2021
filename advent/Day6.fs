[<adventlib.Register.Module>]
module advent.Day6

let PRINT_DAYS = false

type Fish = 
    { timer : byte
    ; spawning : bool
    }

module Fish =
    let init count =
        { timer = count ; spawning = false}
    
    let inits count ready =
        { timer = count ; spawning = ready }
    
    let tick fish =
        match fish.timer with
        | 0uy -> inits 6uy true
        | n -> inits (n - 1uy) false
    
    let spawning fish =
        fish.spawning

    let fmt fish =
        sprintf "%d" fish.timer

    let fmtList fishes =
        fishes
        |> List.map fmt
        |> String.concat ","

    let hatchling = inits 8uy false



let part1 (input : string array) =
    
    let tick fishes =
        let agedFish = List.map Fish.tick fishes
        let numberToSpawn = agedFish |> List.where Fish.spawning |> List.length
        let hatchlings = List.replicate numberToSpawn Fish.hatchling
        List.append agedFish hatchlings
    
    let folder fishes day =
        let population = tick fishes
        if PRINT_DAYS then
            printfn "After %2d days: %s" day (Fish.fmtList population)
        population

    let line = Array.head input
    let fish = line.Split(',')
                |> Array.map byte
                |> Array.map Fish.init
                |> Array.toList
    
    if PRINT_DAYS then
        printfn "Initial state: %s" (Fish.fmtList fish)

    [1..80]
    |> List.fold folder fish
    |> List.length
    |> uint64  // for type matching w/ part2

(*
    For part 2, there are too many individual fish to model without running out of memory. 
    Instead, model groups of fish that are *in the same state.*
    For convenience, re-use our existing Fish record to describe the state of the group
    and just add a field for how many members are in the group at the time.    
*)

type FishGroup =
    { representative : Fish
    ; count : uint64
    }

module FishGroup =
    // This is a "statically resolved type parameter"
    // https://docs.microsoft.com/en-us/dotnet/fsharp/language-reference/generics/statically-resolved-type-parameters
    // At compile time F# emits multiple versions of this function, so "inline" is required.
    // This lets the "count" parameter be any numeric type that is convertible to uint64.
    let inline init< ^T when ^T :
        (static member op_Explicit : ^T -> uint64)> (rep : Fish) (count: ^T) =
            { representative = rep ; count = uint64 count }

    // Helper initializer for creating initial groups
    let fromTimer (timer, count) =
        init (Fish.init timer) count

    // Helper initializer for creating new groups after incrementing existing ones
    let fromRep (rep, count) =
        init rep count

    // Unlike part 1, spawn a while group of new fish at once
    let spawn count = 
        init Fish.hatchling count

    let fmt group =
        sprintf "T%s: %s" (Fish.fmt group.representative) (group.count.ToString("N"))

    let fmtList groups =
        groups
        |> List.sortBy (fun g -> g.representative.timer)
        |> List.map fmt
        |> String.concat " ; "

    // Advance a group by advancing its representative record
    let tick group =
        { group with representative = Fish.tick group.representative }

    // As the simulation progresses, newly added groups will fall into sync with existing ones,
    // i.e. there will be multiple groups with the same state. Assuming this is the case, this
    // function consolidates the groups so there is no more than one per representative state.
    // This isn't really necessary, since without this the max number of groups is the
    // number of simulation days, i.e. the largest our group list could get is the number of
    // starting groups + 256... but I didn't consider that initially, and just went after
    // minimizing the size of the group list.
    let merge groups =
        
        // helper function for List.fold
        let folder (counts : Map<Fish, uint64>) (group : FishGroup) : Map<Fish, uint64> =

            // helper function for Map.change
            let changeFn (oldCount : uint64 option) =
                oldCount
                |> Option.defaultValue 0UL
                |> (+) group.count
                |> Some

            Map.change group.representative changeFn counts 

        // fold the groups into a map keyed by Fish instance,
        // then convert back into a list of FishGroup records
        groups
        |> List.fold folder Map.empty
        |> Map.toList
        |> List.map fromRep

let part2 (input : string array) =
    // instead of modelling each individual fish, model groups of fish
    // where each group has the same timer
    
    let tick groups =
        // advance the current population by one day
        let agedGroups = List.map FishGroup.tick groups
        // see if there's a group at the spawning stage
        let spawningGroup = List.tryFind (fun g -> g.representative.spawning) agedGroups
        match spawningGroup with
        | None -> 
            // no group is spawning, so we don't add any additional groups
            agedGroups
        | Some spawning ->
            // each member of the spawning group will yield a new fish,
            // so create a new group of hatchlings the same size as the
            // spawning group:
            let hatchlings = FishGroup.spawn spawning.count
            // there won't be another group with the same state as the new hatchlings,
            // but there may be older groups that have "synced up", so use merge to
            // de-duplicate the groups and prevent the list size from growing
            FishGroup.merge (hatchlings::agedGroups)

    let folder groups day =
        tick groups

    let groups = Array.head input
                    |> fun l -> l.Split(',')
                    |> Array.map byte
                    |> Array.countBy id
                    |> Array.map FishGroup.fromTimer
                    |> Array.toList

    [1..256]
    |> List.fold (fun gs _ -> tick gs) groups
    |> List.sumBy (fun g -> g.count)

open adventlib

[<Register.Solution(6, Description="Lanternfish")>]
let fullSolution stream = 
    let lines = stream |> Input.getLines |> List.toArray
    Solution.fromFunctions lines [ part1 ; part2 ]
