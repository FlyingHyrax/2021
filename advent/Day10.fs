[<adventlib.Register.Module>]
module advent.Day10

open adventlib

let openingTokens = Set.ofSeq "([{<"
let closingTokens = Set.ofSeq ")]}>"

let (|OpenToken|_|) (c : char) =
    if Set.contains c openingTokens then
        Some c
    else
        None

let (|CloseToken|_|) (c : char) =
    if Set.contains c closingTokens then
        Some c
    else
        None

let expectedClosingToken openingToken =
    match openingToken with
    | '(' -> ')'
    | '[' -> ']'
    | '{' -> '}'
    | '<' -> '>'
    | c -> failwithf "unrecognized character in input: %c" c

let pointsForUnexpectedToken closingToken =
    match closingToken with
    | ')' -> 3
    | ']' -> 57
    | '}' -> 1197
    | '>' -> 25137
    | _   -> 0

let pointsForExpectedToken closingToken =
    match closingToken with
    | ')' -> 1
    | ']' -> 2
    | '}' -> 3
    | '>' -> 4
    | _   -> 0


type CorruptionError =
    { expected : char
    ; actual : char
    }

module CorruptionError =
    let points err = 
        pointsForUnexpectedToken err.actual


type ChunkResult =
    | Valid // turns out, the input never contained any complete lines!
    | Incomplete of char list
    | Corrupted of CorruptionError

module ChunkResult =
    let format chres =
        match chres with
        | Valid -> "valid"
        | Incomplete expected -> sprintf "Incomplete; expecting %A" expected
        | Corrupted err -> sprintf "Corrupt chunk: expected '%c' but found '%c'" err.expected err.actual

    let pickCorrupted chres = 
        match chres with
        | Corrupted err -> Some err
        | _ -> None

    let pickIncomplete chres =
        match chres with
        | Incomplete expected -> Some expected
        | _ -> None


(*
    When we encounter an opening token, push the corresponding closing token onto the stack of expected closing tokens.
    When we encounter a closing token, compare it to the token at the top of the stack. If it matches, a pair was
    closed successfully and we can process the rest of the line; if not then the line is corrupted.

    If we reach the end of a line without encountering a mismatched closing token, either the line was complete and valid
    or the line was incomplete - we can tell by checking the stack. If there are any closing tokens left on the stack then
    there were earlier opening tokens we did not encounter a closing token for.
 *)

let validateLine (line : string) =
    // is this actually tail recursive? don't know, and not sure it matters because the lines are fairly short
    let rec loop (remaining : char list) (expected : char list) : ChunkResult =
        // if we have no more characters to process, there are 2 outcomes:
        let noInputRemaining =
            if List.isEmpty expected then
                // if the expected stack is empty, then we found a matching closing token
                // for every opening one, so the input is valid and complete
                Valid
            else
                // if there are still closing tokens in the stack, then there were opening
                // tokens that had no closing token, so the input is incomplete
                Incomplete expected

        // if we have a closing token, see if it matches the one we're expecting next
        let handleClosingToken c restc =
            match expected with
            | first::restexp -> 
                if first = c then
                    // matched - process the rest of the input
                    loop restc restexp
                else
                    // the closing token didn't match the one we expected to come next
                    Corrupted { expected = first ; actual = c}
            | [] -> 
                // we got a closing token, but there was no matching opening token
                Corrupted { expected = ' '; actual = c }

        let handleCurrentToken c restc =
            match c with
            | OpenToken opTok -> loop restc ((expectedClosingToken opTok)::expected)
            | CloseToken clTok -> handleClosingToken clTok restc
            | _ -> failwith $"neither an opening or closing token? :{c}:"

        match remaining with
        | c::rest -> handleCurrentToken c rest
        | [] -> noInputRemaining
            
    
    loop (List.ofSeq line) List.empty


let syntaxErrorScore (rs : ChunkResult list) =
    List.map ChunkResult.pickCorrupted rs
    |> List.filter Option.isSome
    |> List.map Option.get
    |> List.map CorruptionError.points
    |> List.sum

let completionScore (rs : ChunkResult list) =
    // got an int32 overflow here the first time, multiplication really blows things up
    let scoreRemainingChars (cs : char list) =
        List.fold (fun score c -> 
            (score * 5UL) + uint64(pointsForExpectedToken c)
        ) 0UL cs

    let scores = List.map ChunkResult.pickIncomplete rs
                    |> List.filter Option.isSome
                    |> List.map Option.get
                    |> List.map scoreRemainingChars
                    |> List.sort
    
    // ty, truncting integer division
    let count = List.length scores
    let middle = count / 2
    scores[middle]
    

[<Register.Solution(10, Description="Syntax Scoring")>]
let solve input : Map<string, string>=
    let lines = input |> Input.getLines |> List.map (fun l -> l.TrimEnd())
    let results = List.map validateLine lines

    // validation - dump our result for each line
    results
    |> List.map ChunkResult.format
    |> List.iter (printfn "%s")

    Map.ofList [
        ("Part 1", string <| syntaxErrorScore results) ;
        ("Part 2", string <| completionScore results) ;
    ]
