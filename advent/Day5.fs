[<adventlib.Register.Module>]
module advent.Day5

open FParsec


(* Basic 2D coordinate point *)
type Point2D = int * int

module Point2D =
    (* Accessors to hide the implementation of the Point2D type *)
    
    /// Get the x-value of the point
    let x = fst
    
    /// Get the y-value of the point
    let y = snd
    
    /// Return the point as a coordinate pair (x, y)
    let (|Coord|) (pt : Point2D) =
        x pt, y pt
    
    (* Input/Output for the Point2D type *)

    /// Deserialize a point
    let parser =
        let pair = (pint32 .>> pchar ',') .>>. pint32
        let typed = pair |>> Point2D
        let labelled = typed <?> "Point2D"
        labelled

    
    /// Serialize a point
    let pretty pt =
        sprintf "%d,%d" (x pt) (y pt)


(* Basic 2D line from one point to another *)
type Line2D = Point2D * Point2D

module Line2D =
    
    let first = fst
    let last = snd

    let parser =
        let arrow = pstring " -> "
        let pair = (Point2D.parser .>> arrow) .>>. Point2D.parser
        let typed = pair |>> Line2D
        typed

    let pretty (p1, p2) =
        sprintf "%s -> %s" (Point2D.pretty p1) (Point2D.pretty p2)

    /// Ensure the order of the points such that:
    /// (1) if x1 != x2 then x1 < x2
    /// (2) if x1 = x2 then y1 < y2
    let normalize (p1 : Point2D, p2 : Point2D) : Line2D =
        let (Point2D.Coord (x1, y1)) = p1 
        let (Point2D.Coord (x2, y2)) = p2

        match ((x1, y1), (x2, y2)) with
        | ((x1, _), (x2, _)) when x1 > x2 -> (p2, p1)
        | ((x1, y1), (x2, y2)) when x1 = x2 && y1 > y2 -> (p2, p1)
        | _ -> (p1, p2)

       
    /// Determine the discrete slope of a line, one of:
    /// (1) Horizontal: y1 = y2 (including when p1 = p2!)
    /// (2) Vertical: x1 = x2
    /// (3) Rising: positive slope (dx/dy); as x increases y increases
    /// (4) Falling: negative slope (dx/dy); as x increases y decreases
    let (|Horizontal|Vertical|Rising|Falling|) (p1 : Point2D, p2 : Point2D) =
        let (Point2D.Coord (x1, y1)) = p1
        let (Point2D.Coord (x2, y2)) = p2

        let rise = y2 - y1
        let run = x2 - x1

        let slope (dy : int) (dx : int) : float =
            let fdy = float dy
            let fdx = float dx
            fdy / fdx

        match (rise, run) with
        | (0, _) -> Horizontal
        | (_, 0) -> Vertical
        | (dy, dx) when (slope dy dx) > 0 -> Rising
        | (dy, dx) when (slope dy dx) < 0 -> Falling
        | _ -> failwithf "failed to calculate slope for line %A" (pretty (p1, p2))

    let isHorizontalOrVertical (line : Line2D) : bool =
        match line with
        | Horizontal | Vertical -> true
        | _ -> false

    let allPoints (line : Line2D) =
        let (Point2D.Coord (x1, y1)) = first line
        let (Point2D.Coord (x2, y2)) = last line
        match line with
        | Horizontal ->
            // x increases, y stays constant
            seq { for x in x1..1..x2 -> (x, y1) }
        | Vertical ->
            // y increases, x stays constant
            seq { for y in y1..1..y2 -> (x1, y) }
        | Rising ->
            // both x and y increase
            let xs = seq { for x in x1..1..x2 -> x }
            let ys = seq { for y in y1..1..y2 -> y }
            Seq.zip xs ys
        | Falling ->
            // x increases, y decreases
            let xs = seq { for x in x1..1..x2 -> x }
            let ys = seq { for y in y1..(-1)..y2 -> y }
            Seq.zip xs ys
        |> Seq.map Point2D


let getInputLines (stream : System.IO.Stream) =
    let line = Line2D.parser .>> newline
    let lines = many1 line

    let res = runParserOnStream lines () "" stream System.Text.Encoding.UTF8
    match res with
    | Success (values, _, _) -> values
    | Failure (_, err, _) -> failwith (err.ToString())


let countOverlaps (lines : Line2D list) =
    lines
    |> Seq.map Line2D.allPoints
    |> Seq.concat
    |> Seq.countBy id
    |> Seq.where (fun (_, n) -> n > 1)
    |> Seq.length
    

let part1 (lines: Line2D list) =
    printfn "%d lines read" (List.length lines)
    let nlines = List.map Line2D.normalize lines

    let simpleLines = List.where Line2D.isHorizontalOrVertical nlines
    printfn "%d lines are horizontal or vertical:" (List.length simpleLines)
    // List.iter (Line2D.pretty >> (printfn "%s")) linesOfNote
    
    countOverlaps simpleLines
    
let part2 (lines : Line2D list) =
    lines
    |> List.map Line2D.normalize
    |> countOverlaps
        
open adventlib
[<Register.Solution(5, Description="Hydrothermal venture")>]
let fullSolution stream = 
    let lines = getInputLines stream
    Solution.fromFunctions lines [ part1 ; part2 ]
    