[<adventlib.Register.Module>]
module advent.Day8

open adventlib
open FParsec
open System

type Pattern = Pattern of Set<char>
   
module Pattern =
    let map f p =
        let (Pattern s) = p
        f s

    let fromString (str : string) =
        str.ToCharArray() |> Set.ofArray |> Pattern

    let toString (pat : Pattern) =
        map (Set.toArray >> Array.sort >> String >> string) pat


(* Is there a prettier way of doing this? *)

let decode (recordedPatterns : Pattern list) : List<Pattern * int> =
    
    // Helper function - remove an item from a list
    let removeFromList predicate xs =
        let idx = List.tryFindIndex predicate xs
        match idx with
        | Some i -> List.removeAt i xs
        | None -> xs

    // helper function - get the value from a one-element set
    let exactlyOne xs =
        assert (Seq.length xs = 1)
        List.ofSeq xs |> List.head

    // patterns that are not bound to a digit yet
    let mutable unbound = List.map (fun (Pattern cs) -> cs) recordedPatterns

    // when we deduce a digit, store the pattern at bindings[digit]
    let mutable bindings : Map<int, Set<char>> = Map.empty
    
    // keep our mutation in one place, when we find a digit, add it to
    // the bindings map and remove the corresponding pattern from the unbound list
    // so that we don't match it when looking for subsequent digits
    let set digit pattern =
        // add to bindings table
        bindings <- Map.add digit pattern bindings        
        // remove from unbound list, if present
        unbound <- removeFromList ((=) pattern) unbound

    // 1, 4, 7, 8 have known unique lengths 2, 4, 3, and 7 respectively
    let knownSize len pattern =
        (Set.count pattern) = len
    
    set 1 <| List.find (knownSize 2) unbound
    set 4 <| List.find (knownSize 4) unbound
    set 7 <| List.find (knownSize 3) unbound
    set 8 <| List.find (knownSize 7) unbound
    
    // displays for 1 and 7 differ by only one segment:
    let seg_T = Set.difference bindings[7] bindings[1] |> exactlyOne

    // 9 uses the segments as 1, 4, and 7 plus 1 additional segment at the bottom.
    // So look for a 6-segment pattern where if we take out the segments from 1, 4, and 7
    // we're left with only one other segment.
    let _147 = Set.unionMany [ bindings[1] ; bindings[4] ; bindings[7] ]
    let match9 pattern =
        match Set.count pattern with
        | 6 -> 
            let diff = Set.difference pattern _147
            (Set.count diff) = 1
        | _ -> false

    set 9 <| List.find match9 unbound

    // the additional segment for digit 9 is the bottom segment:
    let seg_B = Set.difference bindings[9] _147 |> exactlyOne

    // Now to distinguish 0 and 6. Both have 6 segments.
    // When we subtract the segments in 6 from the segments in 9, we're left with
    // the top-right segment.
    // When we subtract the segments in 0 from the segments in 9, we're left with
    // the middle segment.
    // Although we don't yet know exactly which of these is which, we can distinguish
    // the 2 results by comparing to the segments in 1!
    // For 6, the remaining segment will be a member of 1's segments.
    // For 0, the remaining segment will NOT be a member of 1's segments.

    let match6 pattern =
        match Set.count pattern with
        | 6 -> 
            let remainder = Set.difference bindings[9] pattern
            Set.isProperSubset remainder bindings[1]
        | _ -> false

    set 6 <| List.find match6 unbound

    // This also gives us the top-right segment - the only segment in 9 that isn't also in 6:
    let seg_TR = Set.difference bindings[9] bindings[6]
                        |> exactlyOne

    // Now 0 is easy, as it's the only 6-segment digit remaining
    set 0 <| List.find (knownSize 6) unbound

    // And as discussed earlier, this gives us the middle segment, as that's the only
    // segment that is in 9 but not in 0:
    let seg_M = Set.difference bindings[9] bindings[0]
                        |> exactlyOne

    // The bottom left segment is the only segment in 0 but not in 9 
    // (the reverse of above, now that we know the pattern for 0)
    let seg_BL = Set.difference bindings[0] bindings[9]
                            |> exactlyOne

    // with the segments we now know, we can deduce some more...
    // we know the top right segment, the bottom right is the other segment used by digit 1:
    let seg_BR = Set.remove seg_TR bindings[1]
                            |> exactlyOne

    // and oh look at that, we know all the segments in digit 3:
    set 3 <| Set.ofList [ seg_T ; seg_M ; seg_B ; seg_TR ; seg_BR ]
    
    // what do we have left? 
    // we've done 0 1 3 4 6 7 8 9
    // leaving 2 5
    // and we know 6 segments, all except top-left.
    // find the segment for top left, and we'll just build the patterns for 2 and 5!

    let seg_TL = bindings[0] - (Set.add seg_BL bindings[3]) |> exactlyOne

    set 2 <| Set.ofList [ seg_T ; seg_TR ; seg_M ; seg_BL ; seg_B ]
    set 5 <| Set.ofList [ seg_T ; seg_TL ; seg_M ; seg_BR ; seg_B ]

    assert (List.isEmpty unbound)
    
    bindings
    |> Map.toList
    |> List.map (fun (d, cs) -> Pattern cs, d)

// parser for a single line of input
let parseEntry =
    // reads a single character from abcdefg
    let letter = anyOf ['a'..'g']
    // reads 1 or more letters in a row as a string
    let pattern = many1Chars letter |>> Pattern.fromString
    // reads 1 or more patterns, separated by spaces, ending in a space
    let leftSide = many1 (pattern .>> (pchar ' '))
    let rightSide = sepBy1 pattern (pchar ' ')
    // read a list of space separated patterns, then the horizontal bar separator, then another list of patterns
    (leftSide .>> pstring "| ") .>>. rightSide

// parser for all lines of input
let parseEntries =
    many1 (parseEntry .>> newline)

// apply parser to input stream
let parseInputFromStream (stream : IO.Stream) =
    let res = runParserOnStream parseEntries () "" stream Text.Encoding.UTF8
    match res with
    | Success (values, _, _) -> values
    | Failure (_, err, _) -> failwith (err.ToString())


let fmtPatterns (ps : List<Pattern * int>) =
    let fmtPattern (p, d) =
        sprintf "%d: %7s" d (Pattern.toString p)
    List.map fmtPattern ps
    |> String.concat "\n"

let processEntry (recordedPatterns : Pattern list, readoutPatterns : Pattern list) =
    let decodedPatterns = decode recordedPatterns
    printfn "%s" <| fmtPatterns decodedPatterns

    let patternToDigit = decodedPatterns
                        |> List.map (fun (p, d) -> Pattern.toString p, d)
                        |> Map.ofList

    let currentDisplay = readoutPatterns
                        |> List.map Pattern.toString
                        |> List.map (fun s -> Map.tryFind s patternToDigit)
                        |> List.map (Option.map (sprintf "%d") >> (Option.defaultValue "?"))
                        |> String.concat " "

    printfn "%s" currentDisplay
    currentDisplay

let part1 entries =
    let processEntry (recordedPatterns : Pattern list, readoutPatterns : Pattern list) =
        let decodedPatterns = decode recordedPatterns

        let patternToDigit = decodedPatterns
                            |> List.map (fun (p, d) -> Pattern.toString p, d)
                            |> Map.ofList
        
        let displayedStrings = readoutPatterns |> List.map Pattern.toString

        let count1478 s =
            match (patternToDigit[s]) with
            | 1 | 4 | 7 | 8 -> 1
            | _ -> 0

        displayedStrings
        |> List.sumBy count1478

    entries
    |> List.map processEntry
    |> List.sum

let part2 entries =
    let processEntry (recordedPatterns : Pattern list, readoutPatterns : Pattern list) =
        
        let patternToDigit = decode recordedPatterns
                            |> List.map (fun (p, d) -> Pattern.toString p, d)
                            |> Map.ofList

        readoutPatterns
        |> List.map Pattern.toString
        |> List.map (fun s -> patternToDigit[s])
        |> List.map (sprintf "%d")
        |> String.concat ""
        |> int
        

    entries
    |> List.map processEntry
    |> List.sum

[<Register.Solution(8, Description="Seven Segment Search")>]
let fullSolution stream =
    let entries = parseInputFromStream stream
    Solution.fromFunctions entries [part1 ; part2]
