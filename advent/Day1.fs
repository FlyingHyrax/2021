namespace advent
open adventlib

[<Register.Module>]
module Day1 =
    type Counter = Option<int> * int

    let compare (previous: int) (current: int) (count: int) : Counter =
        let increase = if current > previous then 1 else 0
        (Some(current), count + increase)
        
    let consume ((previous, count): Counter) (current: int) : Counter =
        match previous with
        | Some(value) -> compare value current count
        | None -> (Some(current), 0)

    let part1 (lines : string array) =
        let (_, result) = lines 
                            |> Array.map int 
                            |> Array.fold consume (None, 0)
        result

    let part2 (lines : string array) =
        let (_, result) = lines
                            |> Array.map int
                            |> Array.windowed 3
                            |> Array.map (Array.reduce (+))
                            |> Array.fold consume (None, 0)
        result

    [<Register.Solution(1, Description="Sonar Sweep")>]
    let solve (input : System.IO.Stream) =
        let lines = Input.getLines input |> List.toArray
        Solution.fromValues [ part1 lines; part2 lines ]
