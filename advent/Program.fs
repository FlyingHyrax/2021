﻿// For more information see https://aka.ms/fsharp-console-apps
open adventlib

// Implicit entry point

// search modules for our custom attributes, and register marked functions as solutions
printf "Loading solutions..."
Register.searchAssemblyForSolutions ()
printfn "finished"

let options = Cli.getProgramOptions ()
let input = Input.openStreamForPath options.path

let day = sprintf "%d" options.day
let solver = Register.getSolution day

match solver with
| None -> printfn $"No solver found for day {day}"
| Some sol ->
        printfn "Day %s : %s" sol.id sol.description
        let solutions = sol.solve input
        Map.iter (fun k v -> printfn "%s: %s" k v) solutions
        printfn ""
