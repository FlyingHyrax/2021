[<adventlib.Register.Module>]
module advent.Day13

open System
open FParsec

// alias away the generic argument for parser user data
type Parser<'t> = Parser<'t, unit>


type Point = int * int

module Point =

    let parse : Parser<Point> =
        (pint32 .>> pchar ',' .>>. pint32) 
        <?> "Expecting a point 'X,Y'"


type Axis =
    | X 
    | Y

module Axis =
    let ofChar (c : char) =
        match c with
        | 'x' -> X
        | 'y' -> Y
        | _ -> failwith "Character must be an 'x' or 'y'"

    let parse : Parser<Axis> =
        (pchar 'x' <|> pchar 'y')
        <?> "Expecting 'x' or 'y'"
        |>> ofChar


type Fold =
    { Axis : Axis
    ; Value : int
    }

module Fold =

    let _parseLine =
        Axis.parse .>> skipChar '=' .>>. pint32
        |>> fun (a, v) -> { Axis = a ; Value = v }

    let parse : Parser<Fold> =
        skipString "fold along " >>. _parseLine


type Document =
    { Points : (int * int) list 
    ; Folds : Fold list
    }

module Document =
    let parse : Parser<Document> =
        let parsePoints = many1 (Point.parse .>> skipNewline)
        let parseFolds = many1 (Fold.parse .>> opt skipNewline)
        pipe2 (parsePoints .>> newline) parseFolds (fun points folds -> 
            { Points = points ; Folds = folds }
        )

    let findGridSize (points : Point list) : int * int =
        let compare (mx, my) (x, y) =
            (max x mx), (max y my)
        List.fold compare (0, 0) points


module Sheet =

    let build (points : Point list) : bool[,] =
        // rows = y, columns = x, so we swap we index with [y, x] not [x, y]
        let mx, my = Document.findGridSize points
        let grid = Array2D.create (my + 1) (mx + 1) false

        for (x, y) in points do
            grid[y, x] <- true

        grid


    let width (grid: 'a[,]) =
        Array2D.length2 grid

    let height (grid: 'a[,]) =
        Array2D.length1 grid

    let toString (grid : bool[,]) : string =
        let formatRow (row : bool[]) : string =
            // ■ ▮ ▯
            row
            |> Array.map (fun b -> if b then '#' else ' ')
            |> String
        
        seq {
            for y = 0 to (height grid - 1) do
                yield formatRow grid[y, *]
        }
        |> String.concat "\n"

    

    let flipHorizontal (grid : bool[,]) =
        // iterate through the rows top to bottom, reversing the elements of each row
        seq {
            for y = 0 to (height grid - 1) do
                yield Array.rev grid[y, *]
        }
        |> array2D


    let flipVertical (grid : bool[,]) =
        // iterate through the rows in reverse order
        seq {
            for y = (height grid - 1) downto 0 do
                yield grid[y, *]
        }
        |> array2D

    let foldLeft (grid : bool[,]) (lineX : int) =
        // split into 2 grids along the fold line
        // b/c the fold is horizontal, we want all the y coordinates and a range of the x coordinates
        let leftHalf = grid[*, 0 .. (lineX - 1)]
        let rightHalf = grid[*, (lineX + 1) .. ] |> flipHorizontal

        // What if the two halves are not the same size?
        // (They are in the example but it's not specified.)
        // Whichever half is smaller, align its right edge to the right edge of the larger half.
        // Iterate over the smaller half and mark dots on the larger half at the overlapping coordinates.
        let overlap (largerHalf : bool[,]) (smallerHalf : bool[,]) : bool[,] =
            // length2 = columns = X = horizontal
            let largerWidth = width largerHalf
            let smallerWidth = width smallerHalf

            let offsetX = largerWidth - smallerWidth

            for srcY = 0 to height smallerHalf - 1 do
                for srcX = 0 to width smallerHalf - 1 do
                    let dstX, dstY = offsetX + srcX, srcY
                    largerHalf[dstY, dstX] <- largerHalf[dstY, dstX] || smallerHalf[srcY, srcX]

            largerHalf

        // whichever is larger will form the bounds of the result grid,
        // and the smaller will be aligned to the right edge of the larger.
        let leftWidth = width leftHalf
        let rightWidth = width rightHalf
        if leftWidth >= rightWidth then
            overlap leftHalf rightHalf
        else
            overlap rightHalf leftHalf

    let foldUp (grid : bool[,]) (lineY : int) =
        // split into 2 grids along the fold line...
        // because we're folding vertically, we want slices with a subset of the y coordinates and all the x coordinates
        let topHalf = grid[0 .. (lineY - 1), *]
        let bottomHalf = grid[(lineY + 1) .. , *] |> flipVertical

        // same idea as the horizontal fold, figure out whichever half is larger and use that for alignment
        // imagining the smaller sheet is aligned to the bottom edge of the larger
        let overlap (largerHalf : bool[,]) (smallerHalf : bool[,]) =
            // length1 = rows = Y = vertical
            let largerHeight = height largerHalf
            let smallerHeight = height smallerHalf

            let offsetY = largerHeight - smallerHeight

            for srcY = 0 to height smallerHalf - 1 do
                for srcX = 0 to width smallerHalf - 1 do
                    let dstX, dstY = srcX, srcY + offsetY
                    largerHalf[dstY, dstX] <- largerHalf[dstY, dstX] || smallerHalf[srcY, srcX]

            largerHalf

        let topHeight = height topHalf
        let bottomHeight = height bottomHalf
        if topHeight >= bottomHeight then
            overlap topHalf bottomHalf
        else
            overlap bottomHalf topHalf

    let doFold (grid : bool[,]) (fold : Fold) =
        let foldFn =
            match fold.Axis with
            // "fold along X = __" indicates a horizontal fold; the fold line itself runs vertically
            // and the sheet is folded "right over left"
            | X -> foldLeft
            // "fold along Y = __" indicates a vertical fold; the fold line itself runs horizontally
            // and the sheet is folded "bottom over top"
            | Y -> foldUp
                
        foldFn grid fold.Value

    let countMarks (grid : bool[,]) =
        seq {
            for y = 0 to height grid - 1 do
                for x = 0 to width grid - 1 do
                    yield grid[y, x]
        }
        |> Seq.map (fun b -> if b then 1 else 0)
        |> Seq.sum

    
[<adventlib.Register.Solution(13, Description="Transparent Origami")>]
let solve (input : IO.Stream) : Map<string, string> =
    let parsedInput = 
        runParserOnStream 
            Document.parse () "stdin" input Text.Encoding.UTF8
    
    match parsedInput with
    | Failure (msg, error, _) ->
        printfn "%s: %A" msg error
        Map.empty
    | Success (document, _, _) ->
        
        // initial grid
        let sheet = Sheet.build document.Points

        // part 1 : do just the first fold, then count the number of holes/marks left
        let afterOne = Sheet.doFold sheet document.Folds.Head
        let p1 = Sheet.countMarks afterOne

        // part 2 : do all the folds, then just print the result so we can see what number is displays
        document.Folds
        |> List.fold Sheet.doFold sheet
        |> Sheet.toString
        |> printfn "%s"
        
        adventlib.Solution.fromValues [ p1 |> string ; "See Logs" ]