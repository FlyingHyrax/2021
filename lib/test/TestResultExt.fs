module TestResultExt

open FsCheck
open FsCheck.Xunit

open adventlib.Result

(* 
    Laws for Functors ("mappables") 
    https://en.wikibooks.org/wiki/Haskell/The_Functor_class#The_functor_laws
    https://fsharpforfunandprofit.com/posts/elevated-world/#the-properties-of-a-correct-map-implementation
*)

[<Property>]
let ``Mappable identity`` (xR : Result<int, string>) =
    let fmap = Result.map
    xR = fmap id xR

[<Property>]
let ``Mappable composition`` (xR : Result<int, string>, f : int -> int, g : int -> string) =
    let fmap = Result.map
    
    let left = fmap (f >> g)
    let right = (fmap f) >> (fmap g)
    
    (left xR) = (right xR)

(* 
    Laws for Applicatives
    https://en.wikibooks.org/wiki/Haskell/Applicative_functors#Applicative_functor_laws
    https://fsharpforfunandprofit.com/posts/elevated-world/#the-properties-of-a-correct-applyreturn-implementation
*)

[<Property>] // "identity"
let ``Applicative over identity`` (xR : Result<int, string>) =
    // Result.pure = Result.Ok
    xR = apply (Ok id) xR

[<Property>] // "homomorphism"
let ``Applicative preserves function application`` (x : int, f : int -> string) =
    let liftBefore = apply (Ok f) (Ok x)
    let liftAfter = Ok (f x)
    liftBefore = liftAfter

[<Property>] // "interchange"
let ``Applicative interchange of application order`` (x : int, f : Result<int -> string, string>) =
    // emulate haskell's '$' operator
    let app arg fn = fn arg
    
    let left = apply f (Ok x)
    let right = apply (Ok (app x)) f
    left = right

[<Property>] // "composition"
let ``Applicative composition order`` (u : Result<float -> string, string>, v : Result<int -> float, string>, w : Result<int, string>) =
    // any time there are multiple associative applications, it's easier to write with infix notation
    let (<*>) = apply
    // emulate haskell's '.' operator
    let compose f g = f << g
    let left = (Ok compose) <*> u <*> v <*> w
    let right = u <*> (v <*> w)
    left = right

[<Property>] // special, applying a lifted function same as fmap
let ``Applicative relationship to functor`` (f : int -> string) (r : Result<int, string>) =
    let fmap = Result.map
    (fmap f r) = apply (Ok f) r

(*
    Laws for monads
    https://fsharpforfunandprofit.com/posts/elevated-world-2/#the-properties-of-a-correct-bindreturn-implementation
    https://en.wikibooks.org/wiki/Haskell/Understanding_monads#Monad_Laws
*)

[<Property>] // "right unit"
let ``Monad return is neutral`` (m : Result<int, string>) =
    let return' = Ok
    let (>>=) r f = Result.bind f r
    m >>= return' = m

[<Property>] // "right unit"
let ``Monad return and bind equivalence to application`` (x : int, f : int -> Result<string, bool>) =
    let return' = Ok
    let (>>=) r f = Result.bind f r
    (return' x >>= f) = f x

[<Property>] // "associativity"
let ``Monad bind associativity`` (m : Result<float, string>) (f : float -> Result<int, string>) (g : int -> Result<string, string>) =
    let (>>=) r f = Result.bind f r
    let groupFromLeft = (m >>= f) >>= g
    let groupFromRight = m >>= (fun v -> (f v) >>= g)
    groupFromLeft = groupFromRight

(* 
    Laws for Traversable
    https://hackage.haskell.org/package/base-4.8.1.0/docs/Data-Traversable.html
    TODO: these have less explanation online and I truly don't know what I'm doing
*)
