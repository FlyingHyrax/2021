namespace adventlib


type Predicate<'a> = 'a -> bool
module Predicate =

    let both (fx : Predicate<'a>) (fy : Predicate<'a>) =
        fun v -> (fx v) && (fy v)

    let all (ps : Predicate<'a> seq) (v : 'a) =
        Seq.forall (fun p -> p v) ps

    let any (ps : Predicate<'a> seq) (v : 'a) =
        Seq.exists (fun p -> p v) ps            

module Seq =
    /// Skip elements from the beginning of a sequence without exploding when the sequence is empty.
    /// For this problem: <https://stackoverflow.com/q/19566140>
    /// This solution: https://stackoverflow.com/a/35733873
    let skipSafe (count : int) (xs : 'a seq) : 'a seq =
        xs
        |> Seq.indexed
        |> Seq.skipWhile (fun (i, _) -> i < count)
        |> Seq.map snd
