namespace adventlib

open System


// Represent a solution we implemented somewhere and tagged with a custom attribute to find later
type Solution =
    { id : string
    ; description : string
    ; solve : (IO.Stream -> Map<string, string>)
    }


// Helper functions for converting existing solution functions to the return type expected by Solution.solve
module Solution =
    let fromValues<'a> (vs : 'a list) : Map<string, string> =
        let one i v =
            let label = sprintf "Part %d" (i + 1)
            let result = string v
            label, result
        vs
        |> List.mapi one
        |> Map.ofList

    let fromFunctions<'i, 'a> (input : 'i) (fns : ('i -> 'a) list) =
        List.map (fun f -> f input) fns
        |> fromValues


// Finding and registering solution functions dynamically
module Register =
    open System.Reflection
    open FSharp.Core
    open FSharp.Reflection


    // Map from arbitrary identifiers to solution records
    type Registry() =
        let mutable registry : Map<string, Solution> = Map.empty

        member __.Add(solver : Solution) =
            registry <- Map.add solver.id solver registry

        member __.Get(id: string) =
            Map.tryFind id registry


    // 'global' store of solutions
    let registry = Registry()


    // for marking modules that contain solutions
    // this does nothing but help us filter out modules at runtime
    [<AttributeUsageAttribute(AttributeTargets.Class ||| AttributeTargets.Assembly, AllowMultiple = false)>]
    type ModuleAttribute() =
        inherit Attribute()


    // for marking solution functions, assumed type IO.Stream -> Map<string, string>
    [<AttributeUsageAttribute(AttributeTargets.Method, AllowMultiple = false, Inherited = false)>]
    type SolutionAttribute(ident: string, description: string) =
        inherit Attribute()

        member val Id: string = ident with get, set 
        member val Description = description with get, set

        new (ident : string) = 
            SolutionAttribute(ident, "")
        
        new(ident : int) =
            SolutionAttribute(sprintf "%d" ident)


    let logMe v =
        printfn "%A" v
        v

    // check whether a member or type has a particular custom attribute applied
    let hasAttribute (attr : Type) (mbr : MemberInfo) =
        let notNull = (isNull >> not)
        mbr.GetCustomAttribute(attr) |> notNull

    // find modules marked with our custom attribute
    let findMarkedModules (assembly : Assembly) =
        assembly.GetExportedTypes()
        |> Array.filter FSharpType.IsModule
        |> Array.filter (hasAttribute typeof<ModuleAttribute>)

    // find functions marked with our custom attribute
    let findMarkedFunctions (t : Type) =
        t.GetMethods()
        |> Array.filter (fun m -> m.IsStatic && m.IsPublic)
        |> Array.filter (hasAttribute typeof<SolutionAttribute>)
        

    // register a reflected method as a solution by pulling metadata from our custom attribute
    // type checking breaks down here! we can't statically verify that the method we've found
    // has the signature we require and have to perform a downcast at runtime
    let registerSolutionMethod (mth : MethodInfo) =
        let attr = mth.GetCustomAttribute<SolutionAttribute>()
        // TODO: can call GetParameters on mth to check that it takes a single stream?
        let sol = { id = attr.Id 
            ; description = attr.Description 
            ; solve = fun stream ->
                downcast mth.Invoke(null, [| stream |])
            }
        registry.Add(sol)

    // search the entry assembly for solution functions and register them
    let searchAssemblyForSolutions () =
        Assembly.GetEntryAssembly()
        |> findMarkedModules
        |> Array.collect findMarkedFunctions
        |> Array.iter registerSolutionMethod

    let getSolution(ident : string) =
        registry.Get(ident)
    
    