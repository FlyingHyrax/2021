﻿namespace adventlib

open System

/// Program command line option parsing
/// Desired program usage:
/// 
///     advent DAY (PATH?)
/// 
/// - day: positive integer: problem to execute solutions for
/// - path: string, optional: path to input file containing plain text problem input
/// 
/// If the "path" argument is not given, then read input from standard input ("stdin") instead
module Cli =

    /// Structure for parsed options
    type Options =
        { day : int
        ; path : string option
        }

    /// Get program arguments, minus the leading program/executable name
    let argv () : string[] =
        Environment.GetCommandLineArgs()[1..]

    /// Try to parse an argument list into an Options record
    let parseOptions (arguments : string list) =
        let tryReadDay =
            try
                arguments
                |> List.item 0
                |> int
                |> fun n -> if n >  0 then 
                                Ok n
                            else
                                Error "'day' argument must be > 0"
            with
            | :? FormatException -> Error $"'day' argument must be an integer"
            | :? ArgumentException -> Error $"not enough arguments - need at least 1 argument for 'day'"

        let tryReadPath =
            try
                match List.tryItem 1 arguments with
                | None -> Ok None
                | Some str -> Ok (Some (IO.Path.GetFullPath(str)))
            with
            | :? ArgumentException as exc -> Error $"Error creating fully-qualified, absolute path for input file: { (exc.Message) }"
            |  exc -> Error exc.Message
        
        tryReadDay |> Result.bind (fun day ->
            tryReadPath |> Result.bind (fun path ->
                Ok { day = day ; path = path }))

    /// Get the programs arguments, parse them into an Options record,
    /// and either return the Options or throw an exception with failure information.
    let getProgramOptions () =
        let args = Array.toList (argv ())
        match parseOptions args with
        | Ok opts -> opts
        | Error err ->
            failwithf "! %s\n!\n! USAGE: advent <day> [<path>]\n!\n! %A\n" err args


/// Input/output helper functions
module Input =
    /// Read lines from standard input as a sequence of strings
    let stdin : string seq =
        // https://codereview.stackexchange.com/a/96498
        fun _ -> Console.ReadLine()
        |> Seq.initInfinite
        |> Seq.takeWhile ((<>) null)

    /// Either open the given file using IO.FileStream, or open standard input as a Stream.
    let openStreamForPath (path : string option) =
        path
        |> Option.map (fun p -> new IO.FileStream(p, IO.FileMode.Open, IO.FileAccess.Read) :> IO.Stream)
        |> Option.defaultWith (fun () -> Console.OpenStandardInput())

    (* 
        Unless we're using FParsec, we'll usually want to either read all the input in as a block of text
        or to iterate over the input line by line. IO.Stream returns bytes - wrapping a Stream in an
        IO.StreamReader creates a TextReader instance that can produce lines/decoded unicode characters instead.
    *)

    /// Get all the input from a stream as a single unicode string
    let getText (stream : IO.Stream) =
        using (new IO.StreamReader(stream)) (fun reader ->
            reader.ReadToEnd()
        )

    /// Get all the input from a stream line-by-line, as a list of unicode strings
    let getLines (stream : IO.Stream) =
        using (new IO.StreamReader(stream)) (fun reader ->
            fun _ -> reader.ReadLine()
            |> Seq.initInfinite
            |> Seq.takeWhile ((<>) null)
            |> Seq.toList
        )
