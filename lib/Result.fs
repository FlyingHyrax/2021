namespace adventlib

module Result =
    // Convert an option to a result by returning the given error value if the option is None
    let fromOption (ifNone : 'TError) (opt : 'T option) : Result<'T, 'TError> =
        match opt with
        | Some x -> Ok x
        | None -> Error ifNone

    /// Implement applicative (maybe?) for Result, which already has bind, return/pure (Result.Ok), and map
    let apply (fnR : Result<'a -> 'b, 'e>) (vR : Result<'a, 'e>) =
        match (fnR, vR) with
        | (Ok fn, Ok v) -> Ok (fn v)
        | Error err, _ -> Error err
        | _, Error err -> Error err

    let lift = Result.map

    let lift2 f x y =
        apply (lift f x) y

    let andThen = Result.bind
    let flatMap = Result.bind

    /// traversal in terms of monad functions (bind/return)
    /// https://fsharpforfunandprofit.com/posts/elevated-world-4/#applicative-vs-monadic-versions-of-traverse
    /// stops on first error, vs. applicative traversal that (could) accumulates errors
    let traverseM (fn : 'a -> Result<'b, 'e> ) (xs : 'a seq) =
        
        // helper function to create a sequence containing a single item
        let one v = seq { yield v }

        // this can be implemented using Seq.fold - you don't need to do tail recursion yourself...
        // the one nice add from this though, is we can terminate early when we hit an error
        let rec loop (accumulator) (remaining) =
            if Seq.isEmpty remaining then
                // Result.Ok equivalent to 'return' for applicatives
                Ok accumulator
            else
                let head = Seq.head remaining
                let tail = Seq.tail remaining

                // this is equivalent to `fn head >>= (fun value -> loop ... tail)` 
                // (i.e. using Result.bind) but I've left it deconstructed for tail-recursion
                match fn head with
                | Error err -> Error err
                | Ok value -> loop (Seq.append accumulator (one value)) tail

        loop Seq.empty xs

    /// sequencing in terms of monad functions (monadic traverse)
    /// since it uses traverseM, stops on first error
    let sequenceM (rs : Result<'t, 'e> seq) : Result<'t seq, 'e> =
        traverseM id rs


    /// Computation expression support for Result
    /// https://docs.microsoft.com/en-us/dotnet/fsharp/language-reference/computation-expressions#creating-a-new-type-of-computation-expression
    
    // use some wrapper types to make our sigantures more explicit
    type Delayed<'T, 'E> = Delayed of (unit -> Result<'T, 'E>)

    type Builder () =
        // This one is straightforward, because this already exists for Result
        // M<'T> * ('T -> M<'U>) -> M<'U>
        member this.Bind (res, fn) =
            Result.bind fn res

        // Similarly, return/pure for Result is just its success case
        // 'T -> M<'T>
        member this.Return (value) =
            Ok value

        // This version is used when the value to return is already a Result,
        // so we don't need to package it into one:
        // M<'T> -> M<'T>
        member this.ReturnFrom (value) =
            value

        // This can be implemented to either return M<'T> or unit -> M<'T>, depending
        // on whether you want immediate or delayed execution. Although I'm not sure
        // if I need it, this is the delayed execution version; in theory this will help
        // in cases where we have a bunch of operations and we don't want to execute
        // later operations if an earlier one failed?
        //
        // (unit -> M<'T>) -> Delayed<'T> where 
        //   Delayed<'T> = (unit -> M<'T>)
        member this.Delay (func) =
            Delayed func

        // If this.Delayed implements delayed execution, then you need to implement
        // Run to trigger execution of a delayed expression.
        // Delayed<'T> -> M<'T>
        member this.Run (Delayed expr) =
            expr()

        // M<'T> * Delayed<'T> -> M<'T>
        // ? there are a lot of ways I could do this and I'm not sure what is best...
        //
        // This doesn't really combine the 2 results (into, say, a list) - instead, if the first operation succeeds
        // (r is Ok) then we ignore its result and return the result of the second expression instead. So if all
        // expressions succeed you'll get the last result, and if one fails you'll get the 'first' failure, with
        // the wierd caveat that combine works bottom to top with >2 expressions...
        member this.Combine (r, Delayed f) =
            this.Bind (r, (fun () -> f ()))

        // With traverse already implemented, this should be what we want?
        // If all the iterations succeed you get a seq of the results, if one of them fails you get the first failure.
        // It could instead produce a list of results, assuming we were doing the same thing with combine.
        member this.For (vs, fR) =
            traverseM fR vs

        // Cheat here and use sequence expressions to create a lazily-evaluated seq<Result>, then 
        // use sequenceM to convert to a Result<seq>, containing either all the Ok values or the first Error
        member this.While (guard, body) =
            seq {
                while guard() do
                    yield body()
            }
            |> sequenceM
            

        // these last 3 are boilerplate
        member this.TryWith (body, handler) =
            try
                this.ReturnFrom(body())
            with
                e -> handler e

        member this.TryFinally (body, finalizer) =
            try
                this.ReturnFrom(body())
            finally
                finalizer()

        member this.Using (disposable : #System.IDisposable, body) =
            let wrapper = fun () -> body disposable
            this.TryFinally(wrapper, fun () -> 
                match disposable with
                | null -> ()
                | disp -> disp.Dispose()
            )

    // syntax sugar for sequencing Result operations (maybe?)
    let computation = new Builder ()
