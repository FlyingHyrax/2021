# Advent of Code 2021

<https://adventofcode.com/2021>

## Environment

So I don't forget what tools I'm using...

- [Windows Terminal](https://github.com/Microsoft/Terminal)
- [.NET 6.0.x](https://dotnet.microsoft.com/en-us/download/dotnet/6.0)
    - [F# 6.x](https://docs.microsoft.com/en-us/dotnet/fsharp/whats-new/fsharp-6)
- [PowerShell Core 7](https://docs.microsoft.com/en-us/powershell/scripting/whats-new/what-s-new-in-powershell-72?view=powershell-7.2)
- [Visual Studio Code](https://code.visualstudio.com/)
    - [ionide-fsharp](https://marketplace.visualstudio.com/items?itemName=Ionide.Ionide-fsharp)

This is essentially the result of following "Option 1" + "Option 2" from here: <https://fsharp.org/use/windows/>

## Organization

- `advent/` - CLI program containing console program and solution modules
- `lib/` - class library for (hopefully) re-usable components like reading input files, organizing solution modules, and type extensions / utilities
- `lib/test/` - companion project for testing utility library with xUnit + FSCheck (pretty much to learn FSCheck)
- `inputs/` - problem input files organized by day

## Build & Run

All the subprojects can be built at once using the solution file in the project root:

```
$ dotnet build
```

To execute a solver, run the 'advent' project with a day number and file path as arguments:

```
$ dotnet run --project .\advent\advent.fsproj <DAY> .\inputs\day<DAY>\<full|sample>.txt
```

If you don't provide a file path, the runner will instead read from standard input:

```
$ gc .\inputs\day1\sample.txt | dotnet run --project .\advent\advent.fsproj 1
```
## Adding Solvers

At the moment all days/problems get their own modules in their own file, e.g. `Day<N>.fs`.

The CLI program uses [custom attributes](https://docs.microsoft.com/en-us/dotnet/standard/attributes/writing-custom-attributes) and some reflection functions in `adventlib.Register` (".\lib\Registry.fs") to mark modules that contain solutions and functions that implement a solution for a particular day.

Modules need to be marked with `adventlib.Register.ModuleAttribute` like so:

```fsharp
// top-level module declaration
[<adventlib.Register.Module>]
module advent.Day10

namespace advent
open adventlib
open System
// ...rest of code
```

...or:

```fsharp
// local module declaration
namespace advent

open adventlib

[<Register.Module>]
module Day10 =
    // ...rest of code
```

Solver functions must have the type signature and have attribute `adventlib.Register.SolutionAttribute`:

```fsharp

// earlier:
open adventlib
open System

// ...later:

[<Register.Solution(10, Description="Problem Title")>]
let solveDayWhatever (input : IO.Stream) : Map<string, string> = // System.IO.Stream -> Map<string, string>
    let linesToProcess = Input.getLines lines

    // ...implement solutions, then return a map of part/solution:

    Map.ofList [
        ("Part 1", string part1solution) ;
        ("Part 2", string part2solution) ;
    ]
```

The attributes are used to find solutions functions at runtime with reflection, mostly as an exercise in how to use `System.Reflection` but also so I don't have to keep them all in a giant static dispatch table somewhere. (And honestly, there's maybe some macro-envy here; I saw someone create a dispatch table at compile time with macros in Zig and isn't that cool?) The number given to `Register.Solution` (e.g. 10 in the example above) is used as the first argument to the CLI program to invoke this solution.

The module `adventlib.Input` contains utilities for transforming the `IO.Stream` into friendlier formats (when we aren't using [FParsec](http://www.quanttec.com/fparsec/about/)).

The module `adventlib.Solution` has functions to help save a couple of characters when returning `Map<string, string>`, e.g.:

```fsharp
[<Register.Solution(101, Description="This is not really a thing")>]
let solveDayWhatever input =
    // process the input into a format both parts of the problem can use
    let sharedInput = parseInputSomehow input
    // passes the input to each function in the list, formats the result into a string, and 
    // auto-generates map keys "Part N" where N is the list index:
    Solution.fromFunctions sharedInput [ functionThatSolvesPart1 ; functionThatSolvesPart2 ]
```
